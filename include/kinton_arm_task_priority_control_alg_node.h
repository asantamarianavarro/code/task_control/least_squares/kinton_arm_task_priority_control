// Copyright (C) 2010-2011 Institut de Robotica i Informatica Industrial, CSIC-UPC.
// Author 
// All rights reserved.
//
// This file is part of iri-ros-pkg
// iri-ros-pkg is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// IMPORTANT NOTE: This code has been generated through a script from the 
// iri_ros_scripts. Please do NOT delete any comments to guarantee the correctness
// of the scripts. ROS topics can be easly add by using those scripts. Please
// refer to the IRI wiki page for more information:
// http://wikiri.upc.es/index.php/Robotics_Lab

#ifndef _kinton_arm_task_priority_control_alg_node_h_
#define _kinton_arm_task_priority_control_alg_node_h_

#include <iri_base_algorithm/iri_base_algorithm.h>
#include "kinton_arm_task_priority_control_alg.h"
#include <urdf/model.h>

// [publisher subscriber headers]
#include <std_msgs/Float64MultiArray.h>
#include <geometry_msgs/TransformStamped.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <visualization_msgs/MarkerArray.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/TwistWithCovariance.h>
#include <ar_tools_msgs/ARMarkers.h>

#include <sstream>

// [service client headers]

// [action server client headers]
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <kinton_msgs/waypointsAction.h>

using namespace KDL;
using namespace Eigen;
using namespace std;


typedef enum  {
    NO_ARM,
    UNINA,
    CATEC,
    IRI,
} arm_type; 

/**
 * \brief IRI ROS Specific Algorithm Class
 *
 */
class KintonArmTaskPriorityControlAlgNode : public algorithm_base::IriBaseAlgorithm<KintonArmTaskPriorityControlAlgorithm>
{
  private:

    bool wait_tstep_for_vel_; // Boolean to wait a time step. Velocity computation is obtained after second position reading.

    // [publisher attributes]
    ros::Publisher debug_data_publisher_;
    std_msgs::Float64MultiArray debug_data_msg_;

    ros::Publisher joint_traj_publisher_;
    trajectory_msgs::JointTrajectory joint_traj_msg_;
    ros::Publisher marker_array_publisher_;
    visualization_msgs::MarkerArray MarkerArray_msg_;

    // [subscriber attributes]
    ros::Subscriber joint_states_subscriber_;
    void joint_states_callback(const sensor_msgs::JointState::ConstPtr& msg);
    pthread_mutex_t joint_states_mutex_;
    void joint_states_mutex_enter(void);
    void joint_states_mutex_exit(void);

    ros::Subscriber pose_subscriber_;
    void pose_callback(const geometry_msgs::TransformStamped::ConstPtr& msg);
    pthread_mutex_t pose_mutex_;
    void pose_mutex_enter(void);
    void pose_mutex_exit(void);

    ros::Subscriber cam_vel_subscriber_;
    void cam_vel_callback(const geometry_msgs::TwistWithCovariance::ConstPtr& msg);
    CMutex cam_vel_mutex_;
    void input_tag_callback(const ar_tools_msgs::ARMarkers::ConstPtr& msg);
    CMutex input_tag_mutex_;
    ros::Subscriber input_tag_subscriber_;

    // [service attributes]

    // [client attributes]

    // [action server attributes]

    // [action client attributes]
    actionlib::SimpleActionClient<kinton_msgs::waypointsAction> waypoints_client_;
    kinton_msgs::waypointsGoal waypoints_goal_;
    bool waypointsMakeActionRequest();
    void waypointsDone(const actionlib::SimpleClientGoalState& state,  const kinton_msgs::waypointsResultConstPtr& result);
    void waypointsActive();
    void waypointsFeedback(const kinton_msgs::waypointsFeedbackConstPtr& feedback);

    // Time vars
    ros::Time time_,time_last_,time_ini_; //Time variables.
    double dt_; // Time differential.
    double curr_t_; // Current execution time.

    // Transforms
    Matrix4d Tcam_in_tip_; // Homogeneous transform of camera in arm tip frames.
    Matrix4d Ttag_in_cam_; // Homogeneous Transform of tag in camera frames.
    Matrix4d Tarmbase_in_baselink_; // Homogeneous Transform of arm base in base link.
    Matrix4d Tlink1_in_armbase_; // Homogeneous Transform of link 1 in arm base link.

    // Matrix gain for VS Jacobian pseudo-inverse
    MatrixXd dist_desired_; // Desired distance vector x,y,z r.t. target.
    MatrixXd dist_desired2_; // Desired distance vector 2 x,y,z r.t. target
    double vs_alpha_min_; // Alpha value for gain matrix pseudo inverse.
    MatrixXd vs_delta_gain_; // Delta values (min and max) for gain matrix pseudo inverse.
    double inf_radius_; // Security distance to an object (inflation radius) to prevent robot from colliding.
    int target_id_; // Target id;
    bool new_marker_; // New marker event;

    bool init_; //Initialize timers and arm parameters.
    bool arm_at_ini_; // Arm at initial position (to avoid multiple publishings)

    // UAM 
    MatrixXd uam_vel_; // velocities (quadrotor and arm joint).
    MatrixXd uam_pos_; // positions (quadrotor and arm joint). 
    MatrixXd uam_new_pos_; // New positions (quadrotor and arm joint) computed using uam_vel obtained from algorithm. 
    MatrixXd uam_pos_ini_; // Initial Quadrotor and arm joint values.

    // Quadrotor
    Eigen::MatrixXd quad_pos_; //Pose of the quadrotor
    Eigen::MatrixXd quad_vel_; //Velocity of the quadrotor
    Eigen::MatrixXd waypoints_; // List of waypoints column-wise. Elements are: x,y,z,yaw,cruise,max_lin_error,max_ang_error,wait.
    int wp_elements_; // To easy a new modification on the waypoints_ number of elements.

    // Arm 
    Chain kdl_chain_; // Arm chain.
    int num_joints_; // Number of joints.
    arm_type arm_type_; // Arm type.
    MatrixXd current_joint_pos_; // Current joint position from real robot.
    MatrixXd current_joint_vel_; // Current joint velocity from real robot.

    bool enable_sec_task_; // Enable secondary task.
    bool cam_vel_ok_; // Check the camera velocities covariance.
    MatrixXd q_des_; // Desired arm positions for secondary task.

    MatrixXd lambda_robot_; // Proportional gain of Robot DOFs.
    MatrixXd gain_sec_task_; // Gains of secondary tasks.

    MatrixXd v_rollpitch_; // Current roll and pitch velocity of the quadrotor from the odometry.
    MatrixXd q_rollpitch_; // Current quadrotor roll and pitch angles from the odometry.
    MatrixXd cam_vel_; // Current velocity of the camera from the visual servo.

    vector<UAM::CArmJoint> joint_info_; // Arm joints info.

    // Saturations
    bool sat_; // Enable saturation.
    MatrixXd sat_quad_; // Quadrotor linear and angular velocity saturations.
    double sat_arm_; // Arm joint velocity saturations.

    // Debug
    bool write_files_; // Enable write some vars to files.
    string folder_name_; // Folder name to write the files.
    std::vector<float> debug_data_; // Vector to publish debug data.

    bool sim_collision_; // To simulate a collision.
    MatrixXd wall_obs_; // Wall obstacle definition First column: position, second: side sizes

  public:
   /**
    * \brief Constructor
    * 
    * This constructor initializes specific class attributes and all ROS
    * communications variables to enable message exchange.
    */
    KintonArmTaskPriorityControlAlgNode(void);

   /**
    * \brief Destructor
    * 
    * This destructor frees all necessary dynamic memory allocated within this
    * this class.
    */
    ~KintonArmTaskPriorityControlAlgNode(void);

  protected:
   /**
    * \brief main node thread
    *
    * This is the main thread node function. Code written here will be executed
    * in every node loop while the algorithm is on running state. Loop frequency 
    * can be tuned by modifying loop_rate attribute.
    *
    * Here data related to the process loop or to ROS topics (mainly data structs
    * related to the MSG and SRV files) must be updated. ROS publisher objects 
    * must publish their data in this process. ROS client servers may also
    * request data to the corresponding server topics.
    */
    void mainNodeThread(void);

   /**
    * \brief dynamic reconfigure server callback
    * 
    * This method is called whenever a new configuration is received through
    * the dynamic reconfigure. The derivated generic algorithm class must 
    * implement it.
    *
    * \param config an object with new configuration from all algorithm 
    *               parameters defined in the config file.
    * \param level  integer referring the level in which the configuration
    *               has been changed.
    */
    void node_config_update(Config &config, uint32_t level);

   /**
    * \brief node add diagnostics
    *
    * In this abstract function additional ROS diagnostics applied to the 
    * specific algorithms may be added.
    */
    void addNodeDiagnostics(void);

    // [diagnostic functions]
    
    // [test functions]

    /**
     * \brief Read parameters
     *
     * In this function all needed parameters are read from parameters server.
     */
    void read_params(void);

   /**
    * \brief Get transform matrix
    *
    * From tf to Eigen 4x4 homogeneous transform
    */
    bool tf_to_eigen(const tf::Transform& transform, Matrix4d& eigenT);

   /**
    * \brief Get transform matrix
    *
    * From Eigen 4x4 to tf homogeneous transform
    */
    void eigen_to_tf(const  Matrix4d& eigenT, tf::Transform& transform);

   /**
    * \brief Broadcast tf
    *
    * Broadcast tf
    */
    void broadcast_tf(const  Matrix4d& eigenT, const std::string& parent, const std::string& child);

   /**
    * \brief Broadcast tf
    *
    * Broadcast tf
    */
    void broadcast_tf(const  tf::Transform& transform, const std::string& parent, const std::string& child);

   /**
    * \brief Arm Initialization
    *
    * Arm Variables initializations. 
    */
    void init_arm(void);

   /**
    * \brief Last published topic initialization
    *
    * Arm Last published topic initialization 
    */
    void init_last_publisher(void);

   /**
    * \brief Get target euclidean distance
    *
    * Get target euclidean distance 
    */
    double target_dist(void);

   /**
    * \brief Get Joints information
    *
    * Get Joints information 
    */
    bool read_joints(urdf::Model &robot_model,const string& root_name,const string& tip_name);

   /**
    * \brief Get Homogeneous Transform between cam and arm tip
    *
    * Get fixed Homogeneous Transform of the camera in tip frames
    */
    Matrix4d get_HT_from_tf(const string& cam_frame, const string& tip_frame);

   /**
    * \brief Output velocity saturations
    *
    * Quadrotor linear and angular and arm joint angular velocity saturations
    */
    void output_vel_saturation(void);

   /**
    * \brief Simulate collision
    *
    * Outputs the distance to collision if a simulation of collision is required
    */
    MatrixXd sim_collision(const MatrixXd& quad_pose);


   /**
    * \brief Fill marker array message
    *
    * Visualization function to fill marker array publisher
    */
    void fill_marker_array(void);

    /**
     * \brief Fill arm joint states message
     *
     * function to fill the arm joint states message
     */
    void fill_arm_joint_states(void);

    /**
     * \brief Fill quadrotor waypoints message
     *
     * function to fill the quadrotor waypoints message
     */
    void fill_quad_wp(void);

    /**
     * \brief Fill joints trajectory message
     *
     * function to fill the joints trajectory message
     */
    void fill_joints_traj(void);

};

#endif
