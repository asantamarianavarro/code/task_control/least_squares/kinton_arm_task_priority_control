#include "kinton_arm_task_priority_control_alg_node.h"

using namespace KDL;
using namespace Eigen;
using namespace std;

/*  Constructor and Destructor */
KintonArmTaskPriorityControlAlgNode::KintonArmTaskPriorityControlAlgNode(void) :
  algorithm_base::IriBaseAlgorithm<KintonArmTaskPriorityControlAlgorithm>(),
  waypoints_client_("waypoints", true)
{
  this->wait_tstep_for_vel_ = true;

  //init class attributes if necessary
  this->loop_rate_ = 20; //in [Hz]

  // Initialize values
  this->init_ = true;
  this->time_ = ros::Time::now();
  this->curr_t_ = 0.0;
  this->dt_ = 0.0;
  this->num_joints_ = 0.0;
  this->cam_vel_ = MatrixXd::Zero(6, 1);
  this->cam_vel_ok_ = false;
  this->v_rollpitch_ = MatrixXd::Zero(2, 1);
  this->q_rollpitch_ = MatrixXd::Zero(2, 1);
  this->sat_quad_ = MatrixXd::Zero(2, 1);
  this->gain_sec_task_ = MatrixXd::Zero(3, 1);

  this->quad_pos_ = Eigen::MatrixXd::Zero(6,1);
  this->quad_vel_ = Eigen::MatrixXd::Zero(6,1);

  this->Ttag_in_cam_ = Eigen::Matrix4d::Identity();
  this->Tcam_in_tip_ = Eigen::Matrix4d::Identity();

  this->target_id_ = 0;

  this->wp_elements_ = 8;
  this->waypoints_ = Eigen::MatrixXd::Zero(this->wp_elements_,1);

  this->time_ini_ = this->time_;

  // Read parameters from ROS parameters server
  read_params();

  this->current_joint_pos_ = MatrixXd::Zero(this->num_joints_,1);
  this->current_joint_vel_ = MatrixXd::Zero(this->num_joints_,1);
  this->uam_pos_ = MatrixXd::Zero(6+this->num_joints_,1);
  this->uam_new_pos_ = MatrixXd::Zero(6+this->num_joints_,1);
  this->uam_vel_ = MatrixXd::Zero(6+this->num_joints_,1);

  // [init publishers]
  this->debug_data_publisher_ = this->public_node_handle_.advertise<std_msgs::Float64MultiArray>("debug_data", 1);
  this->joint_traj_publisher_ = this->public_node_handle_.advertise < trajectory_msgs::JointTrajectory > ("joint_traj", 1);
  this->marker_array_publisher_ = this->public_node_handle_.advertise < visualization_msgs::MarkerArray > ("marker_array", 1);

  // [init subscribers]
  this->joint_states_subscriber_ = this->public_node_handle_.subscribe("joint_states", 1, &KintonArmTaskPriorityControlAlgNode::joint_states_callback, this);
  pthread_mutex_init(&this->joint_states_mutex_,NULL);

  this->pose_subscriber_ = this->public_node_handle_.subscribe("pose", 1, &KintonArmTaskPriorityControlAlgNode::pose_callback, this);
  pthread_mutex_init(&this->pose_mutex_,NULL);

  this->cam_vel_subscriber_ = this->public_node_handle_.subscribe("cam_vel", 1, &KintonArmTaskPriorityControlAlgNode::cam_vel_callback, this);
  this->input_tag_subscriber_ = this->public_node_handle_.subscribe("input_tag", 1, &KintonArmTaskPriorityControlAlgNode::input_tag_callback, this);

  // [init services]

  // [init clients]

  // [init action servers]

  // [init action clients]
}
KintonArmTaskPriorityControlAlgNode::~KintonArmTaskPriorityControlAlgNode(void)
{
  // [free dynamic memory]
  pthread_mutex_destroy(&this->joint_states_mutex_);
  pthread_mutex_destroy(&this->pose_mutex_);
}

/*  Main node thread  */
void KintonArmTaskPriorityControlAlgNode::mainNodeThread(void)
{

  // Shared variables	
  this->cam_vel_mutex_.enter();
  bool cam_vel_ok = this->cam_vel_ok_;
  MatrixXd cam_vel = this->cam_vel_;
  this->cam_vel_mutex_.exit();

  this->input_tag_mutex_.enter();
  Matrix4d Ttag_in_cam = this->Ttag_in_cam_;
  double t_dist = target_dist();  // distance to target
  bool init = this->init_;
  this->input_tag_mutex_.exit();

  // get real robot positions
  this->joint_states_mutex_enter();
  this->pose_mutex_enter();
  this->uam_pos_.block(0,0,6,1) = this->quad_pos_;
  this->pose_mutex_exit(); 
  this->uam_pos_.block(6,0,this->num_joints_,1) = this->current_joint_pos_;
  this->joint_states_mutex_exit();

  // Use computed poses (opne loop)
  // this->uam_pos_ = this->uam_new_pos_;

  // Initialize arm and time
  if (init)
  {
    // Initialize arm joints
    init_arm();

    // Initialize robot velocities
    this->uam_vel_ = MatrixXd::Zero(6 + this->num_joints_, 1);
    this->wait_tstep_for_vel_ = true;

    // Fill arm joints trajectory
    fill_joints_traj();
    this->joint_traj_publisher_.publish(this->joint_traj_msg_);

    this->input_tag_mutex_.enter();
    this->time_last_ = this->time_; // Reset time flags
    this->init_ = false;
    this->input_tag_mutex_.exit();
  }
  else
  {
    // get time differential
    this->input_tag_mutex_.enter();
    this->dt_ = (this->time_ - this->time_last_).toSec();
    this->time_last_ = this->time_;
    this->input_tag_mutex_.exit();

    // Quadrotor distance to obstacles
    this->pose_mutex_enter();
    MatrixXd quad_dist_obs = MatrixXd::Zero(3,1);
    this->sim_collision_ = false;
    if (this->sim_collision_)
      quad_dist_obs = sim_collision(this->quad_pos_);
    this->pose_mutex_exit(); 

    if (cam_vel_ok && this->arm_type_ != NO_ARM && !this->wait_tstep_for_vel_)
    {

      // current time
      // this->curr_t_ = this->curr_t_ + this->dt_;
      this->curr_t_ = (ros::Time::now()-this->time_ini_).toSec();

      this->alg_.lock();

      // Goal related parameters
      this->alg_.goal_obj_.cam_vel = cam_vel;
      this->alg_.goal_obj_.target_dist = t_dist;

      // Set control parameters
      this->alg_.ctrl_params_.lambda_robot = this->lambda_robot_;
      this->alg_.ctrl_params_.enable_sec_task = this->enable_sec_task_;
      this->alg_.ctrl_params_.q_rollpitch = this->q_rollpitch_;
      this->alg_.ctrl_params_.v_rollpitch = this->v_rollpitch_;
      this->alg_.ctrl_params_.jntlim_pos_des = this->q_des_;
      this->alg_.ctrl_params_.dt = this->dt_;
      this->alg_.ctrl_params_.ir_gain = this->gain_sec_task_(0,0);
      this->alg_.ctrl_params_.cog_gain = this->gain_sec_task_(1,0);
      this->alg_.ctrl_params_.jntlim_gain = this->gain_sec_task_(2,0);
      this->alg_.ctrl_params_.vs_alpha_min = this->vs_alpha_min_;
      this->alg_.ctrl_params_.vs_delta_gain = this->vs_delta_gain_;
      this->alg_.ctrl_params_.inf_radius = this->inf_radius_; 
    
      // Set Homogeneous transformations
      this->alg_.HT_.cam_in_tip = this->Tcam_in_tip_;
      this->alg_.HT_.tag_in_cam = Ttag_in_cam;
      this->alg_.HT_.armbase_in_baselink = this->Tarmbase_in_baselink_;
      this->alg_.HT_.link1_in_armbase = this->Tlink1_in_armbase_;


    
      // Arm data
      this->alg_.arm_.chain = this->kdl_chain_;
      this->alg_.arm_.joint_info= this->joint_info_;
      this->alg_.arm_.jnt_pos = this->uam_pos_.block(6,0,this->num_joints_,1);

      // Quadrotor data
      this->alg_.quad_.pos = this->uam_pos_.block(0,0,6,1);

      // Main algorithm
      this->alg_.task_priority_ctrl(quad_dist_obs, this->write_files_, this->folder_name_, this->curr_t_, this->uam_pos_, this->uam_vel_, this->debug_data_);

      this->alg_.unlock();

      // Publish debug data
      this->debug_data_msg_.layout.dim.resize(1);
      this->debug_data_msg_.layout.dim[0].label = "";
      this->debug_data_msg_.layout.dim[0].size = this->debug_data_.size();
      this->debug_data_msg_.layout.dim[0].stride = this->debug_data_.size();
      this->debug_data_msg_.layout.data_offset = 0;

      this->debug_data_msg_.data.resize(this->debug_data_.size());
      for (int ii = 0; ii < this->debug_data_.size(); ++ii)
        this->debug_data_msg_.data[ii] = this->debug_data_.at(ii);
      this->debug_data_publisher_.publish(this->debug_data_msg_);


      // Positions increment
      this->uam_new_pos_ = this->uam_pos_+this->uam_vel_ * this->dt_;

      // DEBUG
      // this->alg_.lock();
      // broadcast_tf(this->alg_.HT_.tip_in_baselink,"/kinton/base_link","/debug_tip"); // Check Kinematics
      // broadcast_tf(this->alg_.HT_.tag_in_baselink,"/kinton/base_link","/debug_taget"); 
      // broadcast_tf(this->alg_.arm_.T_base_to_joint.at(0),"/kinton/base_link","/debug_armbase");
      // broadcast_tf(this->alg_.arm_.T_base_to_joint.at(1),"/kinton/base_link","/debug_l1");
      // broadcast_tf(this->alg_.arm_.T_base_to_joint.at(2),"/kinton/base_link","/debug_l2");
      // broadcast_tf(this->alg_.arm_.T_base_to_joint.at(3),"/kinton/base_link","/debug_l3");
      // broadcast_tf(this->alg_.arm_.T_base_to_joint.at(4),"/kinton/base_link","/debug_l4");
      // broadcast_tf(this->alg_.arm_.T_base_to_joint.at(5),"/kinton/base_link","/debug_l5");
      // broadcast_tf(this->alg_.arm_.T_base_to_joint.at(6),"/kinton/base_link","/debug_l6");
      // this->alg_.unlock();

      // Saturate output if required
      if (this->sat_)
        output_vel_saturation();

      // Fill marker array
      fill_marker_array();
      this->marker_array_publisher_.publish(this->MarkerArray_msg_);

      // [fill msg structures]

      // [fill srv structure and make request to the server]

      // [fill action structure and make request to the action server]

      // fill quadrotor velocity msg
      fill_quad_wp();
      waypointsMakeActionRequest();

      // Fill arm joints trajectory
      fill_joints_traj();
      this->joint_traj_publisher_.publish(this->joint_traj_msg_);

      this->cam_vel_mutex_.enter();
      this->cam_vel_ok_ = false;
      this->cam_vel_mutex_.exit();
    }
    else
    {
      this->wait_tstep_for_vel_ = false;

      // Timeout without tag  
      if (ros::Time::now().toSec() - this->time_last_.toSec() > 1.0)
      {
        this->input_tag_mutex_.enter();
        this->init_ = true;
        this->input_tag_mutex_.exit();
      }
    }
  }

  // [publish messages]
  // Uncomment the following line to publish the topic message
  //this->debug_data_publisher_.publish(this->debug_data_Float64MultiArray_msg_);

}

/*  [subscriber callbacks] */
void KintonArmTaskPriorityControlAlgNode::joint_states_callback(const sensor_msgs::JointState::ConstPtr& msg)
{
  ROS_DEBUG("KintonArmTaskPriorityControlAlgNode::joint_states_callback: New Message Received");

  //use appropiate mutex to shared variables if necessary
  //this->alg_.lock();
  this->joint_states_mutex_enter();

  for (int ii = 0; ii < msg->name.size(); ++ii)
  {
    stringstream jname;
    jname << "joint" << ii+1;
    if (jname.str().compare(msg->name[ii]) == 0)
    {
      //double old_pos = this->current_joint_pos_(ii,0);
      this->current_joint_pos_(ii,0) = msg->position[ii];
      //if (std::abs(this->current_joint_pos_(ii,0)-old_pos)>0.5)
      //{
      //  this->current_joint_pos_(ii,0) = old_pos;   
      //}  
      this->current_joint_vel_(ii,0) = msg->velocity[ii];
    }
  }
   
  //unlock previously blocked shared variables
  //this->alg_.unlock();
  this->joint_states_mutex_exit();
}

void KintonArmTaskPriorityControlAlgNode::joint_states_mutex_enter(void)
{
  pthread_mutex_lock(&this->joint_states_mutex_);
}

void KintonArmTaskPriorityControlAlgNode::joint_states_mutex_exit(void)
{
  pthread_mutex_unlock(&this->joint_states_mutex_);
}

void KintonArmTaskPriorityControlAlgNode::pose_callback(const geometry_msgs::TransformStamped::ConstPtr& msg)
{
  ROS_DEBUG("KintonArmTaskPriorityControlAlgNode::pose_callback: New Message Received");

  //use appropiate mutex to shared variables if necessary
  //this->alg_.lock();
  this->pose_mutex_enter();

  this->quad_pos_(0,0) = msg->transform.translation.x;
  this->quad_pos_(1,0) = msg->transform.translation.y;
  this->quad_pos_(2,0) = msg->transform.translation.z;

  double roll, pitch,yaw;
  tf::Quaternion qt;
  tf::quaternionMsgToTF(msg->transform.rotation,qt);
  tf::Matrix3x3(qt).getRPY(roll,pitch,yaw);

  this->quad_pos_(3,0) = roll;
  this->quad_pos_(4,0) = pitch;
  this->quad_pos_(5,0) = yaw;

  //unlock previously blocked shared variables
  //this->alg_.unlock();
  this->pose_mutex_exit();
}
void KintonArmTaskPriorityControlAlgNode::pose_mutex_enter(void)
{
  pthread_mutex_lock(&this->pose_mutex_);
}
void KintonArmTaskPriorityControlAlgNode::pose_mutex_exit(void)
{
  pthread_mutex_unlock(&this->pose_mutex_);
}

void KintonArmTaskPriorityControlAlgNode::cam_vel_callback(const geometry_msgs::TwistWithCovariance::ConstPtr& msg)
{
  ROS_DEBUG("KintonArmTaskPriorityControlAlgNode::twist_cov_callback: New Message Received");

  //lock to prevent issues with shared variables
  this->cam_vel_mutex_.enter();

  // Check if covariance is acceptable (<100 according to IRI visual servoing nodes)
  if (msg->covariance[0] < 1000)
  {
    this->cam_vel_(0, 0) = msg->twist.linear.x;
    this->cam_vel_(1, 0) = msg->twist.linear.y;
    this->cam_vel_(2, 0) = msg->twist.linear.z;
    this->cam_vel_(3, 0) = msg->twist.angular.x;
    this->cam_vel_(4, 0) = msg->twist.angular.y;
    this->cam_vel_(5, 0) = msg->twist.angular.z;
    this->cam_vel_ok_ = true;
  }
  else
  {
    this->cam_vel_ = MatrixXd::Zero(6, 1);
  }

  //unlock previously blocked shared variables
  this->cam_vel_mutex_.exit();
}
void KintonArmTaskPriorityControlAlgNode::input_tag_callback(const ar_tools_msgs::ARMarkers::ConstPtr& msg)
{
  ROS_DEBUG("KintonVsControlAlgNode::input_tag_callback: New Message Received");

  //lock to prevent issues with shared variables
  this->input_tag_mutex_.enter();

  // Check marker existence and no NaN orientation values
  if (!msg->markers.empty() && (!std::isnan(msg->markers[0].pose.pose.orientation.x) && std::isfinite(msg->markers[0].pose.pose.orientation.x)))
  {
    this->time_ = msg->markers.back().header.stamp;
    geometry_msgs::Pose pose = msg->markers.back().pose.pose;
    this->target_id_ = msg->markers.back().id;

    // Convert pose from marker ROS msg to Eigen transform.
    tf::Transform tf_pose;
    tf::poseMsgToTF(pose, tf_pose);
    bool T_ok = tf_to_eigen(tf_pose,this->Ttag_in_cam_);
    if (!T_ok)
    {
      this->time_ = ros::Time::now();
      this->init_ = true;
      ROS_INFO("Wrong tf to Eigen transform");
    }
  }
  else
  {
    this->Ttag_in_cam_ = Matrix4d::Identity();
    this->time_ = ros::Time::now();
    this->init_ = true;
    ROS_INFO("No tag detected");
  }

  //unlock previously blocked shared variables
  this->input_tag_mutex_.exit();
}

/*  [service callbacks] */

/*  [action callbacks] */
void KintonArmTaskPriorityControlAlgNode::waypointsDone(const actionlib::SimpleClientGoalState& state,  const kinton_msgs::waypointsResultConstPtr& result)
{
  alg_.lock();
  if( state == actionlib::SimpleClientGoalState::SUCCEEDED )
    ROS_DEBUG("[kinton_arm_task_priority_control_alg_node]: waypointsDone: Goal Achieved!");
  else
    ROS_DEBUG("[kinton_arm_task_priority_control_alg_node]: waypointsDone: %s", state.toString().c_str());

  //copy & work with requested result
  alg_.unlock();
}
void KintonArmTaskPriorityControlAlgNode::waypointsActive()
{
  alg_.lock();
  //ROS_INFO("KintonArmTaskPriorityControlAlgNode::waypointsActive: Goal just went active!");
  alg_.unlock();
}
void KintonArmTaskPriorityControlAlgNode::waypointsFeedback(const kinton_msgs::waypointsFeedbackConstPtr& feedback)
{
  alg_.lock();
  //ROS_INFO("KintonArmTaskPriorityControlAlgNode::waypointsFeedback: Got Feedback!");

  bool feedback_is_ok = true;

  //analyze feedback
  //my_var = feedback->var;

  //if feedback is not what expected, cancel requested goal
  if( !feedback_is_ok )
  {
    waypoints_client_.cancelGoal();
    //ROS_INFO("KintonArmTaskPriorityControlAlgNode::waypointsFeedback: Cancelling Action!");
  }
  alg_.unlock();
}

/*  [action requests] */
bool KintonArmTaskPriorityControlAlgNode::waypointsMakeActionRequest()
{
  // IMPORTANT: Please note that all mutex used in the client callback functions
  // must be unlocked before calling any of the client class functions from an
  // other thread (MainNodeThread).
  // this->alg_.unlock();
  if(waypoints_client_.isServerConnected())
  {

    MatrixXd waypoints = this->waypoints_;
  
    int num_wp = waypoints.cols();
  
    waypoints_goal_.waypoints.resize(num_wp);
  
    for (int wp = 0; wp < num_wp; ++wp)
    {
      waypoints_goal_.waypoints[wp].pose.position.x = waypoints(0,wp);
      waypoints_goal_.waypoints[wp].pose.position.y = waypoints(1,wp);
      waypoints_goal_.waypoints[wp].pose.position.z = waypoints(2,wp);
  
      tf::Quaternion qt;
      qt.setRPY(0.0,0.0,waypoints(3,wp));
      geometry_msgs::Quaternion qt_msg;
      tf::quaternionTFToMsg(qt,qt_msg);
      waypoints_goal_.waypoints[wp].pose.orientation = qt_msg;

      waypoints_goal_.waypoints[wp].cruise = waypoints(4,wp);
      waypoints_goal_.waypoints[wp].max_confidence_error[0] = waypoints(5,wp);
      waypoints_goal_.waypoints[wp].max_confidence_error[1] = waypoints(6,wp);
      waypoints_goal_.waypoints[wp].wait = waypoints(7,wp);
    }

    //ROS_DEBUG("KintonArmTaskPriorityControlAlgNode::waypointsMakeActionRequest: Server is Available!");
    //send a goal to the action server
    //waypoints_goal_.data = my_desired_goal;
    waypoints_client_.sendGoal(waypoints_goal_,
                boost::bind(&KintonArmTaskPriorityControlAlgNode::waypointsDone,     this, _1, _2),
                boost::bind(&KintonArmTaskPriorityControlAlgNode::waypointsActive,   this),
                boost::bind(&KintonArmTaskPriorityControlAlgNode::waypointsFeedback, this, _1));
    // this->alg_.lock();
    // ROS_DEBUG("KintonArmTaskPriorityControlAlgNode::MakeActionRequest: Goal Sent.");
    return true;
  }
  else
  {
    // this->alg_.lock();
    // ROS_DEBUG("KintonArmTaskPriorityControlAlgNode::waypointsMakeActionRequest: HRI server is not connected");
    return false;
  }
}


/*  Constructor initializations  */
void KintonArmTaskPriorityControlAlgNode::read_params(void)
{
  // Get arm model
  bool arm_unina, arm_catec, arm_iri;
  this->public_node_handle_.param<bool>("arm_unina", arm_unina, false);
  this->public_node_handle_.param<bool>("arm_catec", arm_catec, false);
  this->public_node_handle_.param<bool>("arm_iri", arm_iri, false);
  if (arm_unina)
    this->arm_type_ = UNINA;
  else if (arm_catec)
    this->arm_type_ = CATEC;
  else if (arm_iri)
    this->arm_type_ = IRI;

  // Get enable secondary tasks
  this->public_node_handle_.param<bool>("enable_sec_task",this->enable_sec_task_, true);

  // Get saturation values
  this->public_node_handle_.param<bool>("sat_enable", this->sat_, false);
  this->public_node_handle_.param<double>("sat_quad_linear",	this->sat_quad_(0, 0), 0);
  this->public_node_handle_.param<double>("sat_quad_angular",this->sat_quad_(1, 0), 0);
  this->public_node_handle_.param<double>("sat_arm_joint_vel", this->sat_arm_, 0);

  this->public_node_handle_.param<bool>("write_files", this->write_files_,false);
  this->public_node_handle_.param <string> ("folder_name", this->folder_name_, "/Desktop/Results");

  // Get desired position vector
  this->dist_desired_ = MatrixXd::Zero(3, 1);
  this->public_node_handle_.param<double>("des_x", this->dist_desired_(0, 0),0);
  this->public_node_handle_.param<double>("des_y", this->dist_desired_(1, 0),0);
  this->public_node_handle_.param<double>("des_z", this->dist_desired_(2, 0),0);
  this->dist_desired2_ = MatrixXd::Zero(3, 1);
  this->public_node_handle_.param<double>("des2_x", this->dist_desired2_(0, 0),0);
  this->public_node_handle_.param<double>("des2_y", this->dist_desired2_(1, 0),0);
  this->public_node_handle_.param<double>("des2_z", this->dist_desired2_(2, 0),0);

  // Get values for VS inverse gain matrix
  this->vs_delta_gain_ = MatrixXd::Zero(2, 1);
  this->public_node_handle_.param<double>("W_alpha_min", this->vs_alpha_min_,-0.1);
  this->public_node_handle_.param<double>("W_min", this->vs_delta_gain_(0, 0),0.2);
  this->public_node_handle_.param<double>("W_max", this->vs_delta_gain_(1, 0),1.5);

  // Get values for security measure (minimum distance to the ground)
  this->public_node_handle_.param<double>("Inf_radius", this->inf_radius_,0.2);

  // Get quadrotor and arm initial values
  this->uam_pos_ini_ = MatrixXd::Zero(12, 1);
  this->public_node_handle_.param<double>("quad_x_ini", this->uam_pos_ini_(0, 0), 0);
  this->public_node_handle_.param<double>("quad_y_ini", this->uam_pos_ini_(1, 0), 0);
  this->public_node_handle_.param<double>("quad_z_ini", this->uam_pos_ini_(2, 0),2.5);
  this->public_node_handle_.param<double>("quad_roll_ini", this->uam_pos_ini_(3, 0), 0);
  this->public_node_handle_.param<double>("quad_pitch_ini", this->uam_pos_ini_(4, 0), 0);
  this->public_node_handle_.param<double>("quad_yaw_ini", this->uam_pos_ini_(5, 0), 0);
  this->public_node_handle_.param<double>("arm_q1_ini", this->uam_pos_ini_(6, 0), 0);
  this->public_node_handle_.param<double>("arm_q2_ini", this->uam_pos_ini_(7, 0),0.2);
  this->public_node_handle_.param<double>("arm_q3_ini", this->uam_pos_ini_(8, 0),1.7);
  this->public_node_handle_.param<double>("arm_q4_ini", this->uam_pos_ini_(9, 0), 2);
  this->public_node_handle_.param<double>("arm_q5_ini", this->uam_pos_ini_(10, 0), 0);
  this->public_node_handle_.param<double>("arm_q6_ini", this->uam_pos_ini_(11, 0), 0);

  string robot_prefix;
  if (!this->public_node_handle_.getParam("robot_prefix", robot_prefix))
    ROS_ERROR("No robot_prefix found on parameter server");

  // Arm desired position for the secondary task of Joint limits
  // this->q_des_ = MatrixXd::Zero(5,1);

  // this->public_node_handle_.param<double>("third_task_q1", this->q_des_(0,0), 0);
  // this->public_node_handle_.param<double>("third_task_q2", this->q_des_(1,0), 0.2812);
  // this->public_node_handle_.param<double>("third_task_q3", this->q_des_(2,0), 0.908);
  // this->public_node_handle_.param<double>("third_task_q4", this->q_des_(3,0), 1.975);
  // this->public_node_handle_.param<double>("third_task_q5", this->q_des_(4,0), 0);

  // Best
  // this->q_des_ << 0, 0.2812, 0.908, 1.975, 0;

  // URDF Model information
  ros::NodeHandle nh;

  string tip_name, root_name, optical_name;
  string xml_string;
  Tree tree;
  string urdf_xml, full_urdf_xml;
  nh.param("urdf_xml", urdf_xml, string("robot_description"));
  nh.searchParam(urdf_xml, full_urdf_xml);
  ROS_DEBUG("Reading xml file from parameter server\n");
  if (!nh.getParam(full_urdf_xml, xml_string))
    ROS_ERROR("Could not load the xml from parameter server: %s\n",urdf_xml.c_str());
  if (!nh.getParam("root_name", root_name))
    ROS_ERROR("No root name found on parameter server");
  if (!nh.getParam("tip_name", tip_name))
    ROS_ERROR("No tip name found on parameter server");

  // KDL Parser to create a KDL chain
  if (!kdl_parser::treeFromString(xml_string, tree))
    ROS_ERROR("Could not initialize tree object");
  if (!tree.getChain(root_name, tip_name, this->kdl_chain_))
    ROS_ERROR("Could not initialize kdl_chain object");

  // Get Joints Information
  urdf::Model robot_model;
  if (!robot_model.initString(xml_string))
    ROS_ERROR("Could not initialize robot model");

  if (!read_joints(robot_model, root_name, tip_name))
    ROS_ERROR("Could not read information about the joints");
  if (!nh.getParam("optical_frame_id", optical_name))
    ROS_ERROR("No camera optical frame name found on parameter server");

  std::stringstream tf_tip,tf_optical;
  tf_tip << robot_prefix << "/" << tip_name;
  tf_optical << robot_prefix << "/" << optical_name;

  // Vars depending on number of joints
  this->public_node_handle_.param<bool>("enable_sec_task", this->enable_sec_task_, true);
  this->public_node_handle_.param<double>("gain_sec_task_IR", this->gain_sec_task_(0,0) , 1.0);
  this->public_node_handle_.param<double>("gain_sec_task_CoG", this->gain_sec_task_(1,0) , 1.0);
  this->public_node_handle_.param<double>("gain_sec_task_jntlim", this->gain_sec_task_(2,0) , 1.0);

  double lambda_quad, lambda_arm;
  this->public_node_handle_.param<double>("lambda_quadrotor", lambda_quad, 1.0);
  this->public_node_handle_.param<double>("lambda_arm", lambda_arm, 1.0);

  this->lambda_robot_ = MatrixXd::Zero(4 + this->num_joints_, 1);
  for (int ii = 0; ii < 4; ++ii)
    this->lambda_robot_(ii, 0) = lambda_quad;
  for (int ii = 0; ii < this->num_joints_; ++ii)
    this->lambda_robot_(4 + ii, 0) = lambda_arm;


  // Get Homogenous transform
  this->Tcam_in_tip_ = get_HT_from_tf(tf_optical.str(), tf_tip.str());
  this->Tarmbase_in_baselink_ =  get_HT_from_tf("/kinton/arm_base","/kinton/base_link");
  this->Tlink1_in_armbase_ =  get_HT_from_tf("/kinton/link1","/kinton/base_link");
}
bool KintonArmTaskPriorityControlAlgNode::read_joints(urdf::Model &robot_model, const string& root_name, const string& tip_name)
{
  int num_joints = 0;

  // get joint maxs and mins
  boost::shared_ptr<const urdf::Link> link = robot_model.getLink(tip_name);
  boost::shared_ptr<const urdf::Joint> joint;

  while (link && link->name != root_name)
  {
    joint = robot_model.getJoint(link->parent_joint->name);
    if (!joint)
    {
      ROS_ERROR("Could not find joint: %s", link->parent_joint->name.c_str());
      return false;
    }
    if (joint->type != urdf::Joint::UNKNOWN && joint->type != urdf::Joint::FIXED)
    {
      ROS_DEBUG( "read_joints: adding joint: [%s]", joint->name.c_str() );
      num_joints++;
    }
    link = robot_model.getLink(link->getParent()->name);
  }

  this->num_joints_ = num_joints;
  this->joint_info_.resize(num_joints);

  link = robot_model.getLink(tip_name);
  int ii = 0;
  while (link && ii < num_joints)
  {
    joint = robot_model.getJoint(link->parent_joint->name);
    if (joint->type != urdf::Joint::UNKNOWN && joint->type != urdf::Joint::FIXED)
    {
      ROS_DEBUG( "read_joints: getting bounds for joint: [%s]", joint->name.c_str() );
      float lower, upper;
      if (joint->type != urdf::Joint::CONTINUOUS)
      {
        lower = joint->limits->lower;
        upper = joint->limits->upper;
      }
      else
      {
        lower = -M_PI;
        upper = M_PI;
      }
      int index = num_joints - ii - 1;
      this->joint_info_.at(index).joint_min = lower;
      this->joint_info_.at(index).joint_max = upper;
      this->joint_info_.at(index).link_child = link->name;
      this->joint_info_.at(index).link_parent = link->getParent()->name;
      ii++;
    }
    link = robot_model.getLink(link->getParent()->name);
  }
  return true;
}
Matrix4d KintonArmTaskPriorityControlAlgNode::get_HT_from_tf(const string& cam_frame, const string& tip_frame)
{
  Matrix4d Tcam_in_tip = Matrix4d::Identity();

  bool transform_ok = false;
  
  tf::TransformListener listener;
  tf::StampedTransform transform;
  
  while (!transform_ok)
  {
    try
    {
      listener.waitForTransform(tip_frame, cam_frame, ros::Time(0),	ros::Duration(1.0));
      listener.lookupTransform(tip_frame, cam_frame, ros::Time(0), transform);
      transform_ok = true;
    }
    catch (tf::TransformException ex)
    {
      ROS_ERROR("[kinton_arm_task_priority_control_alg_node]: Kinton Transform error: %s", ex.what());
    }
  }
  // tf transform to Eigen homogenous matrix
  bool T_ok = tf_to_eigen(transform,Tcam_in_tip);
  if (!T_ok)
    ROS_ERROR("get_HT_from_tf: Wrong tf to Eigen transform");
  
  return Tcam_in_tip;
}

/*  Misc. functions  */
bool KintonArmTaskPriorityControlAlgNode::tf_to_eigen(const tf::Transform& transform, Matrix4d& eigenT)
{
  double roll, pitch, yaw;
  transform.getBasis().getRPY(roll, pitch, yaw);

  // Euler ZYX convention
  Matrix3d Rot;
  Rot = AngleAxisd(yaw, Vector3d::UnitZ())
      * AngleAxisd(pitch, Vector3d::UnitY())
      * AngleAxisd(roll, Vector3d::UnitX());

  bool T_ok = false;

  eigenT = Matrix4d::Identity();
  if (!std::isnan(Rot(0, 0)))
  {
    eigenT.block(0, 0, 3, 3) = Rot;
    eigenT(0, 3) = transform.getOrigin().x();
    eigenT(1, 3) = transform.getOrigin().y();
    eigenT(2, 3) = transform.getOrigin().z();
    T_ok = true;
  }

  return T_ok;
}
void KintonArmTaskPriorityControlAlgNode::eigen_to_tf(const  Matrix4d& eigenT, tf::Transform& transform)
{
  double x,y,z,roll,pitch,yaw;
  x = eigenT(0,3);
  y = eigenT(1,3);
  z = eigenT(2,3);
  Matrix3d Rot = eigenT.block(0,0,3,3);
  // Matrix3d Rot = eigenT.block(0,0,3,3).transpose();
  Vector3d angles = Rot.eulerAngles(2, 1, 0);

  transform.setOrigin( tf::Vector3(x, y, z) );
  tf::Quaternion q;
  q.setRPY(angles(2,0),angles(1,0),angles(0,0));
  transform.setRotation(q);
}
void KintonArmTaskPriorityControlAlgNode::broadcast_tf(const  Matrix4d& eigenT, const std::string& parent, const std::string& child)
{
  tf::Transform transform;
  eigen_to_tf(eigenT,transform);
  broadcast_tf(transform,parent,child);
}
void KintonArmTaskPriorityControlAlgNode::broadcast_tf(const  tf::Transform& transform, const std::string& parent, const std::string& child)
{
  static tf::TransformBroadcaster br;
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), parent, child));
}

void KintonArmTaskPriorityControlAlgNode::init_arm()
{
  this->uam_new_pos_ = MatrixXd::Zero(6 + this->num_joints_, 1);
  this->uam_new_pos_.block(0, 0, 6, 1) = this->uam_pos_ini_.block(0, 0, 6, 1);
  this->q_des_ = MatrixXd::Zero(this->num_joints_, 1);

  switch (this->arm_type_) {
    case UNINA:
      this->q_des_ << 0.0, -0.3, 1.0, 1.45, 0.0;
      this->uam_new_pos_.block(6, 0, this->num_joints_, 1) << 0.0, -0.5, 0.5, 1.7, 0.0;
      //this->uam_new_pos_.block(6, 0, this->num_joints_, 1) << 0.0, 1.5, 2.5, 0.0, 0.0;
      break;
    case CATEC:
      this->q_des_ << 0.0, 2.0, -1.0, 0.0, 0.0, 0.758;
      this->uam_new_pos_.block(6, 0, this->num_joints_, 1) << 0.0, 2.0, -1.0, 0.0, 0.0, 0.758;
      break;
    case IRI:
      this->q_des_ << 0.0, 0.0, 1.3, 2.3, -1.8, 0.0;
      this->uam_new_pos_.block(6, 0, this->num_joints_, 1) << 0.0, 0.0, 0.0, 1.745, 1.0, 0.0;
      break;			
    default:
      ROS_ERROR("Wrong arm configuration");
      break;
  }
}
double KintonArmTaskPriorityControlAlgNode::target_dist()
{
  double dist;
  MatrixXd desired;

  if (this->target_id_ == 0)
    desired = this->dist_desired_;
  if (this->target_id_ == 1)
    desired = this->dist_desired2_;

  double x = this->Ttag_in_cam_(0, 3) - desired(0, 0);
  double y = this->Ttag_in_cam_(1, 3) - desired(1, 0);
  double z = this->Ttag_in_cam_(2, 3) - desired(2, 0);

  dist = sqrt((x * x) + (y * y) + (z * z));

  return dist;
}
void KintonArmTaskPriorityControlAlgNode::output_vel_saturation()
{
  double diff;
  MatrixXd sat_quad(this->sat_quad_);

  // Quad Velocities
  for (int ii = 0; ii < 3; ++ii)
  {   
    if (this->uam_vel_(ii,0) > sat_quad(0, 0))
      this->uam_vel_(ii,0) = sat_quad(0, 0);
    else if (this->uam_vel_(ii,0) < -sat_quad(0, 0))
      this->uam_vel_(ii,0) = -sat_quad(0, 0);
    if (this->uam_vel_(3+ii,0) > sat_quad(1, 0))
      this->uam_vel_(3+ii,0) = sat_quad(1, 0);
    else if (this->uam_vel_(ii,0) < -sat_quad(1, 0))
      this->uam_vel_(3+ii,0) = -sat_quad(1, 0);    
  }  

  // Arm joints
  for (int ii = 0; ii < this->num_joints_; ++ii)
  {   
    if (this->uam_vel_(6+ii,0) > this->sat_arm_)
      this->uam_pos_(6+ii,0) = this->uam_pos_(6+ii,0) + this->sat_arm_*this->dt_;
    else if (this->uam_vel_(6+ii,0) < -this->sat_arm_)
      this->uam_pos_(6+ii,0) = this->uam_pos_(6+ii,0) - this->sat_arm_*this->dt_;
  }
}

MatrixXd KintonArmTaskPriorityControlAlgNode::sim_collision(const MatrixXd& quad_pose)
{
  MatrixXd quad_dist_obs = MatrixXd::Zero(3,1);
  // Simulate Ground distance as obstacle
  // quad_dist_obs = quad_pose.block(0,0,3,1);

  // // Simulate obstacle: sphere with center at [-0.5,0.2,1.0] with radius r=0.1
  // quad_dist_obs(0,0) = std::abs(quad_pose(0,0)-(-0.5));
  // quad_dist_obs(1,0) = std::abs(quad_pose(1,0)-(0.2));
  // quad_dist_obs(2,0) = std::abs(quad_pose(2,0)-(1.0));

  // Simulate wall at the left: with dimensions:
  // x=[-0.6 -0.4], y=[0.1], z=[0 2]
  this->wall_obs_ = MatrixXd::Zero(3,2);
  this->wall_obs_(0,0) = -0.5;
  this->wall_obs_(0,1) = 0.1;
  this->wall_obs_(1,0) = 0.2;
  this->wall_obs_(1,1) = 0.1;
  this->wall_obs_(2,0) = 1.0;
  this->wall_obs_(2,1) = 2.0;

  MatrixXd vec_coll = MatrixXd::Zero(3,1);
  for (int ii = 0; ii < 3; ++ii)
    vec_coll(ii,0) = this->wall_obs_(ii,0) - quad_pose(ii,0);

  if (vec_coll.norm()<this->inf_radius_)
    quad_dist_obs(1,0) = vec_coll(1,0);

  return quad_dist_obs;
}

/*  Fill publishers  */
void KintonArmTaskPriorityControlAlgNode::fill_marker_array()
{
  this->MarkerArray_msg_.markers.clear();


  // ARM COG
  visualization_msgs::Marker cog_marker;
  cog_marker.header.frame_id = "/kinton/base_link";
  cog_marker.header.stamp = this->time_;
  cog_marker.ns = "cog";
  cog_marker.type = visualization_msgs::Marker::SPHERE;
  cog_marker.action = visualization_msgs::Marker::ADD;
  cog_marker.lifetime = ros::Duration();
  cog_marker.id = 0;

  double cog_x = this->alg_.htpriority_ctrl.arm_cog_data_.arm_cog(0,0);
  double cog_y = this->alg_.htpriority_ctrl.arm_cog_data_.arm_cog(1,0);
  double cog_z = this->alg_.htpriority_ctrl.arm_cog_data_.arm_cog(2,0);

  cog_marker.pose.position.x = cog_x;
  cog_marker.pose.position.y = cog_y;
  cog_marker.pose.position.z = cog_z;

  cog_marker.pose.orientation.x = 0.0;
  cog_marker.pose.orientation.y = 0.0;
  cog_marker.pose.orientation.z = 0.0;
  cog_marker.pose.orientation.w = 1.0;

  cog_marker.scale.x = 0.025;
  cog_marker.scale.y = 0.025;
  cog_marker.scale.z = 0.025;

  double xy_dist = std::sqrt(std::pow(cog_x,2) + std::pow(cog_y,2));
  double e_radius = 0.02;

  if (xy_dist > e_radius)
  {
    cog_marker.color.r = 1.0;
    cog_marker.color.g = 0.0;
    cog_marker.color.b = 0.0;
  }
  else
  {
    cog_marker.color.r = xy_dist/e_radius;
    cog_marker.color.g = 1-(xy_dist/e_radius);
    cog_marker.color.b = 0.0;
  }


  cog_marker.color.a = 1.0;

  this->MarkerArray_msg_.markers.push_back(cog_marker);

  // Link COG
  for (int ii = 0; ii < this->alg_.htpriority_ctrl.arm_cog_data_.link_cog.size(); ++ii)
  {
    visualization_msgs::Marker cog_link_marker;
    cog_link_marker.header.frame_id = "/kinton/base_link";
    cog_link_marker.header.stamp = this->time_;
    cog_link_marker.ns = "cog";
    cog_link_marker.type = visualization_msgs::Marker::SPHERE;
    cog_link_marker.action = visualization_msgs::Marker::ADD;
    cog_link_marker.lifetime = ros::Duration();
    cog_link_marker.id = ii+1;
  
    cog_link_marker.pose.position.x = this->alg_.htpriority_ctrl.arm_cog_data_.link_cog.at(ii)(0,0);
    cog_link_marker.pose.position.y = this->alg_.htpriority_ctrl.arm_cog_data_.link_cog.at(ii)(1,0);
    cog_link_marker.pose.position.z = this->alg_.htpriority_ctrl.arm_cog_data_.link_cog.at(ii)(2,0);
  
    cog_link_marker.pose.orientation.x = 0.0;
    cog_link_marker.pose.orientation.y = 0.0;
    cog_link_marker.pose.orientation.z = 0.0;
    cog_link_marker.pose.orientation.w = 1.0;
  
    cog_link_marker.scale.x = 0.01;
    cog_link_marker.scale.y = 0.01;
    cog_link_marker.scale.z = 0.01;
    cog_link_marker.color.r = 1.0f;
    cog_link_marker.color.g = 0.4f;
    cog_link_marker.color.b = 0.0f;
    cog_link_marker.color.a = 1.0;
  
    this->MarkerArray_msg_.markers.push_back(cog_link_marker);
  }

  // Obstacle 
  if (this->sim_collision_)
  {
    visualization_msgs::Marker obstacle_marker;
    obstacle_marker.header.frame_id = "/optitrak";
    obstacle_marker.header.stamp = this->time_;
    obstacle_marker.ns = "obstacle";
    obstacle_marker.type = visualization_msgs::Marker::CUBE;
    obstacle_marker.action = visualization_msgs::Marker::ADD;
    obstacle_marker.lifetime = ros::Duration();
    obstacle_marker.id = this->MarkerArray_msg_.markers.size()+1;
  
    obstacle_marker.pose.position.x = this->wall_obs_(0,0);
    obstacle_marker.pose.position.y = this->wall_obs_(1,0);
    obstacle_marker.pose.position.z = this->wall_obs_(2,0);
  
    obstacle_marker.pose.orientation.x = 0.0;
    obstacle_marker.pose.orientation.y = 0.0;
    obstacle_marker.pose.orientation.z = 0.0;
    obstacle_marker.pose.orientation.w = 1.0;
  
    obstacle_marker.scale.x = this->wall_obs_(0,1);
    obstacle_marker.scale.y = this->wall_obs_(1,1);
    obstacle_marker.scale.z = this->wall_obs_(2,1);
    obstacle_marker.color.r = 1.0f;
    obstacle_marker.color.g = 0.0f;
    obstacle_marker.color.b = 0.0f;
    obstacle_marker.color.a = 1.0;
  
    this->MarkerArray_msg_.markers.push_back(obstacle_marker);    
  }


}
void KintonArmTaskPriorityControlAlgNode::fill_quad_wp()
{
  this->pose_mutex_enter();
  Eigen::MatrixXd quad_pos = this->quad_pos_;
  this->pose_mutex_exit();

  this->quad_vel_ = this->uam_vel_.block(0,0,6,1);

  this->waypoints_(0,0) = quad_pos(0,0) + this->quad_vel_(0,0);
  this->waypoints_(1,0) = quad_pos(1,0) + this->quad_vel_(1,0);
  this->waypoints_(2,0) = quad_pos(2,0) + this->quad_vel_(2,0);
  this->waypoints_(3,0) = quad_pos(5,0) + this->quad_vel_(5,0);  
  this->waypoints_(4,0) = 0.3;
  this->waypoints_(5,0) = 0.05;
  this->waypoints_(6,0) = 0.05;
  this->waypoints_(7,0) = 0.0;
}
void KintonArmTaskPriorityControlAlgNode::fill_joints_traj()
{
  this->joint_traj_msg_.header.stamp = ros::Time::now();
  this->joint_traj_msg_.header.frame_id = "arm";
  this->joint_traj_msg_.joint_names.resize(this->num_joints_);
  this->joint_traj_msg_.points.resize(1);
  this->joint_traj_msg_.points[0].positions.resize(this->num_joints_);
  this->joint_traj_msg_.points[0].velocities.resize(this->num_joints_);
  this->joint_traj_msg_.points[0].accelerations.resize(this->num_joints_);
  this->joint_traj_msg_.points[0].effort.resize(this->num_joints_);
  this->joint_traj_msg_.points[0].time_from_start = ros::Duration(1.0);

  for (int jj = 0; jj < this->num_joints_; ++jj)
  {
    stringstream name;
    name << "joint" << jj + 1;
    this->joint_traj_msg_.joint_names[jj] = name.str();
    this->joint_traj_msg_.points[0].positions[jj] = this->uam_new_pos_(6 + jj, 0);
    this->joint_traj_msg_.points[0].velocities[jj] = 0.0;
    this->joint_traj_msg_.points[0].accelerations[jj] = 0.0;
    this->joint_traj_msg_.points[0].effort[jj] = 1.0;
  }
}

/*  Node admin  */
void KintonArmTaskPriorityControlAlgNode::node_config_update(Config &config, uint32_t level)
{
  this->alg_.lock();
  this->alg_.unlock();
}
void KintonArmTaskPriorityControlAlgNode::addNodeDiagnostics(void)
{
}

/*  Main function  */
int main(int argc, char *argv[])
{
	return algorithm_base::main < KintonArmTaskPriorityControlAlgNode > (argc, argv, "kinton_arm_task_priority_control_alg_node");
}
