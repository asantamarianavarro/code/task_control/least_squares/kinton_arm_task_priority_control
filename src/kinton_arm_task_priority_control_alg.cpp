#include "kinton_arm_task_priority_control_alg.h"

using namespace KDL;
using namespace Eigen;

KintonArmTaskPriorityControlAlgorithm::KintonArmTaskPriorityControlAlgorithm(void)
{
}

KintonArmTaskPriorityControlAlgorithm::~KintonArmTaskPriorityControlAlgorithm(void)
{
}

void KintonArmTaskPriorityControlAlgorithm::config_update(Config& new_cfg, uint32_t level)
{
  this->lock();

  // save the current configuration
  this->config_=new_cfg;
  
  this->unlock();
}

// KintonArmTaskPriorityControlAlgorithm Public API

void KintonArmTaskPriorityControlAlgorithm::task_priority_ctrl(const MatrixXd& quad_dist_obs,
                                                               const bool& write_files, 
                                                               const string& folder_name,
                                                               const double& ts,
                                                               MatrixXd& robot_pos, 
                                                               MatrixXd& robot_vel,
                                                               std::vector<float>& debug_data)
{

  // Main low-level algorithm
  htpriority_ctrl.htpc(quad_dist_obs,this->goal_obj_,this->HT_,this->arm_,this->quad_,this->ctrl_params_,robot_pos,robot_vel);

  // Arm torque

  // double rr = sqrt(this->ctrl_params_.cog_arm(0,0)*this->ctrl_params_.cog_arm(0,0)+
  //   this->ctrl_params_.cog_arm(1,0)*this->ctrl_params_.cog_arm(1,0)+
  //   this->ctrl_params_.cog_arm(2,0)*this->ctrl_params_.cog_arm(2,0));
  // double rr = sqrt(this->ctrl_params_.cog_arm(0,0)*this->ctrl_params_.cog_arm(0,0)+
  //   this->ctrl_params_.cog_arm(1,0)*this->ctrl_params_.cog_arm(1,0));
  double rr = sqrt(this->ctrl_params_.cog_arm(0,0)*this->ctrl_params_.cog_arm(0,0)+
    this->ctrl_params_.cog_arm(1,0)*this->ctrl_params_.cog_arm(1,0));    
  double dot_product = this->ctrl_params_.cog_arm(0,0);
  double angle = asin(dot_product/rr);
  // Robot mass
  double FF = 1.8326;
  MatrixXd torque = MatrixXd::Zero(1,1);
  torque(0,0) = rr * FF * sin(-angle);

  // Store debug data
  debug_data.clear();

  debug_data.push_back((float)ts);

  // ROBOT _______________________________________
  // robot pose
  for (int ii = 0; ii < robot_pos.rows(); ++ii)
    debug_data.push_back((float)robot_pos(ii,0));
  // robot velocity
  for (int ii = 0; ii < robot_vel.rows(); ++ii)
    debug_data.push_back((float)robot_vel(ii,0));

  // SECONDARY TASKS______________________________
  // IR task velocity
  for (int ii = 0; ii < this->ctrl_params_.ir_vel.rows(); ++ii)
    debug_data.push_back((float)this->ctrl_params_.ir_vel(ii,0));
  // VS task velocity
  for (int ii = 0; ii < this->ctrl_params_.vs_vel.rows(); ++ii)
    debug_data.push_back((float)this->ctrl_params_.vs_vel(ii,0));
  // CoG task velocity
  for (int ii = 0; ii < this->ctrl_params_.cog_vel.rows(); ++ii)
    debug_data.push_back((float)this->ctrl_params_.cog_vel(ii,0));
  // JL task velocity
  for (int ii = 0; ii < this->ctrl_params_.jntlim_vel.rows(); ++ii)
    debug_data.push_back((float)this->ctrl_params_.jntlim_vel(ii,0));
  // IR_SEC_TASK_____________
  // Inflation radius
  debug_data.push_back((float)this->ctrl_params_.inf_radius);  
  // Y distance to obstacle
  debug_data.push_back((float)quad_dist_obs(1,0));  
  // COG_SEC_TASK_____________
  // Arm CoG
  for (int ii = 0; ii < this->ctrl_params_.cog_arm.rows(); ++ii)
    debug_data.push_back((float)this->ctrl_params_.cog_arm(ii,0));
  // Arm torque
  debug_data.push_back(torque(0,0));
  // JL_SEC_TASK_____________
  // JL desired positions
  for (int ii = 0; ii < this->ctrl_params_.jntlim_pos_des.rows(); ++ii)
    debug_data.push_back((float)this->ctrl_params_.jntlim_pos_des(ii,0));
  // JL Position error
  for (int ii = 0; ii < this->ctrl_params_.jntlim_pos_error.rows(); ++ii)
    debug_data.push_back((float)this->ctrl_params_.jntlim_pos_error(ii,0)); 

  //Eigenvalues (if published)
  for (int ii = 0; ii < htpriority_ctrl.eig_values_.vs.rows(); ++ii)
    debug_data.push_back((float)htpriority_ctrl.eig_values_.vs(ii)); 
  for (int ii = 0; ii < htpriority_ctrl.eig_values_.cog.rows(); ++ii)
    debug_data.push_back((float)htpriority_ctrl.eig_values_.cog(ii)); 
  for (int ii = 0; ii < htpriority_ctrl.eig_values_.jl.rows(); ++ii)
    debug_data.push_back((float)htpriority_ctrl.eig_values_.jl(ii));         

  // Jacobians
  for (int ii = 0; ii < htpriority_ctrl.jacobians_.ir.rows(); ++ii)
    for (int jj = 0; jj < htpriority_ctrl.jacobians_.ir.cols(); ++jj)
      debug_data.push_back((float)htpriority_ctrl.jacobians_.ir(ii,jj));
  for (int ii = 0; ii < htpriority_ctrl.jacobians_.vs.rows(); ++ii)
    for (int jj = 0; jj < htpriority_ctrl.jacobians_.vs.cols(); ++jj)
      debug_data.push_back((float)htpriority_ctrl.jacobians_.vs(ii,jj));
  for (int ii = 0; ii < htpriority_ctrl.jacobians_.cog.rows(); ++ii)
    for (int jj = 0; jj < htpriority_ctrl.jacobians_.cog.cols(); ++jj)
      debug_data.push_back((float)htpriority_ctrl.jacobians_.cog(ii,jj));
  for (int ii = 0; ii < htpriority_ctrl.jacobians_.jl.rows(); ++ii)
    for (int jj = 0; jj < htpriority_ctrl.jacobians_.jl.cols(); ++jj)
      debug_data.push_back((float)htpriority_ctrl.jacobians_.jl(ii,jj));

  if (write_files)
  {

    // General
    UAM::CCommon_Fc::write_to_file(folder_name,"robot_pos.txt",robot_pos,ts);
    UAM::CCommon_Fc::write_to_file(folder_name,"robot_vel.txt",robot_vel,ts);

    // tpc
    stringstream tpc_folder;
    tpc_folder << folder_name << "/tpc";
    UAM::CCommon_Fc::write_to_file(tpc_folder.str(),"vel_ir.txt",this->ctrl_params_.ir_vel,ts);
    UAM::CCommon_Fc::write_to_file(tpc_folder.str(),"vel_vs.txt",this->ctrl_params_.vs_vel,ts);
    UAM::CCommon_Fc::write_to_file(tpc_folder.str(),"vel_cog.txt",this->ctrl_params_.cog_vel,ts);
    UAM::CCommon_Fc::write_to_file(tpc_folder.str(),"vel_jntlim.txt",this->ctrl_params_.jntlim_vel,ts);
    
    //IR specific
    MatrixXd infrad(1,1),quadheight(1,1);
    infrad(0,0) = this->ctrl_params_.inf_radius;
    quadheight(0,0) = quad_dist_obs(1,0);
    UAM::CCommon_Fc::write_to_file(tpc_folder.str(),"inf_radius.txt",infrad,ts);
    UAM::CCommon_Fc::write_to_file(tpc_folder.str(),"ground_dist.txt",quadheight,ts);
   
    //Jntlim specific
    UAM::CCommon_Fc::write_to_file(tpc_folder.str(),"jntlim_pos_des.txt",this->ctrl_params_.jntlim_pos_des,ts);
    UAM::CCommon_Fc::write_to_file(tpc_folder.str(),"jntlim_error.txt",this->ctrl_params_.jntlim_pos_error,ts);

    // CoG specific
    UAM::CCommon_Fc::write_to_file(tpc_folder.str(),"arm_cog.txt",this->ctrl_params_.cog_arm,ts);
    UAM::CCommon_Fc::write_to_file(tpc_folder.str(),"arm_torque.txt",torque,ts);

    //Main Homogenous Transforms
    stringstream ht_folder;
    ht_folder << folder_name << "/htransf";
    UAM::CCommon_Fc::write_to_file(ht_folder.str(),"tag_in_cam.txt",this->HT_.tag_in_cam,ts);
    UAM::CCommon_Fc::write_to_file(ht_folder.str(),"tag_in_baselink.txt",this->HT_.tag_in_baselink,ts);
    UAM::CCommon_Fc::write_to_file(ht_folder.str(),"cam_in_tip.txt",this->HT_.cam_in_tip,ts);
    UAM::CCommon_Fc::write_to_file(ht_folder.str(),"tip_in_baselink.txt",this->HT_.tip_in_baselink,ts);
    UAM::CCommon_Fc::write_to_file(ht_folder.str(),"cam_in_baselink.txt",this->HT_.cam_in_baselink,ts);
    UAM::CCommon_Fc::write_to_file(ht_folder.str(),"armbase_in_baselink.txt",this->HT_.armbase_in_baselink,ts);

  }
}
