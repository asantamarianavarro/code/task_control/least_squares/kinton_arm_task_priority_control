# ROS Kinton-arm Task Priority Control

ROS wrapper to work with [uam_task_ctrl](https://gitlab.com/asantamarianavarro/code/task_control/least_squares/uam_task_ctrl) library. 

**Related publications:** 

[Uncalibrated Visual Servo for Unmanned Aerial Manipulation](http://www.angelsantamaria.eu/publications/tmech17)

A. Santamaria-Navarro, P. Grosch, V. Lippiello, J. Solà and J. Andrade-Cetto

IEEE/ASME Transactions on Mechatronics. To appear. 

### Software dependencies

##### C++ uam_task_ctrl library and dependencies

- [Boost](http://www.boost.org/) (already installed in most Ubuntu LTS)
  
- [Orocos - KDL](http://www.orocos.org/kdl)

- [Eigen 3](eigen.tuxfamily.org)

- [iriutils](https://gitlab.iri.upc.edu/labrobotica/algorithms/iriutils)

    
##### ROS dependencies

- [tf](http://wiki.ros.org/tf), [std_msgs](http://wiki.ros.org/std_msgs), 
[sensor_msgs](http://wiki.ros.org/sensor_msgs), 
[trajectory_msgs](http://wiki.ros.org/trajectory_msgs), 
[visualization_msgs](http://wiki.ros.org/visualization_msgs), 
[geometry_msgs](http://wiki.ros.org/geometry_msgs), 
[kdl_parser](http://wiki.ros.org/kdl_parser), 
[eigen_conversions](http://wiki.ros.org/eigen_conversions),
[actionlib](http://wiki.ros.org/eigen_conversions)

- [iri_algorithm](https://gitlab.iri.upc.edu/labrobotica/ros/iri_core/iri_base_algorithm) - IRI ROS metapackage

  - Move to workspace: `roscd && cd ../src`
  - Download the library: `git clone https://gitlab.iri.upc.edu/labrobotica/ros/iri_core/iri_base_algorithm.git`
  - Compile: `roscd && cd .. && catkin_make` 

- [kinton_msgs](https://devel.iri.upc.edu/labrobotica/ros/iri-ros-pkg_hydro/metapackages/kinton_robot/kinton_msgs) - IRI ROS Kinton msgs

  - Move to workspace: `roscd && cd ../src`
  - Download the library: `svn co https://devel.iri.upc.edu/labrobotica/ros/iri-ros-pkg_hydro/metapackages/kinton_robot/kinton_msgs`
  - Compile: `roscd && cd .. && catkin_make` 
  
- [ar_tools_msgs](https://gitlab.com/asantamarianavarro/code/marker_detectors/ARTools)

  - Move to workspace: `roscd && cd ../src`
  - Download the library: `git clone https://gitlab.com/asantamarianavarro/code/marker_detectors/ARTools.git`
  - Compile: `roscd && cd .. && catkin_make` 

### Installation

- Move to workspace: `roscd && cd ../src`
- Clone the repository: `https://gitlab.com/asantamarianavarro/code/task_control/least_squares/kinton_arm_task_priority_control.git`
- Compile:  `roscd && cd .. && catkin_make -DCMAKE_BUILD_TYPE=Release`

### Example of usage

- Run `roslaunch kinton_arm_task_priority_control kinton_arm_task_priority_control.launch`.

### Support material and multimedia

Please, visit: [**asantamaria's web page**](http://www.angelsantamaria.eu)