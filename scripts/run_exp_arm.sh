#!/bin/bash

# Open or close roscore
roscore () {
	if [ ${1} == "on" ]; then
		# Open Roscore
		x-terminal-emulator -e "roscore" &
		# Wait time
		sleep 5
	elif [ ${1} == "off" ]; then
		# kill roscore
		ps -ef | grep "roscore" | awk '{print $2}' | xargs kill
	fi
}

# Enable or Disable secondary task in Launch file
up_enable_sec_task () {
	if [ ${2} == "true" ]; then
		str_activate_old="\"enable_sec_task_\"       default=\"false\""  
		str_activate_new="\"enable_sec_task_\"       default=\"${2}\"" 
		sed -i -e "s/$str_activate_old/$str_activate_new/g" ${1}
	else
		str_activate_old="\"enable_sec_task_\"       default=\"true\""  
		str_activate_new="\"enable_sec_task_\"       default=\"${2}\"" 
		sed -i -e "s/$str_activate_old/$str_activate_new/g" ${1}
	fi
}

# Lunch File: Robot Initial Positions
up_old_launch_var () {
	# Initial Launch Values
	old_quad_x=${1}
	old_quad_y=${2}
	old_quad_z=${3}
	old_arm_q1=${4}
	old_arm_q2=${5}
	old_arm_q3=${6}
	old_arm_q4=${7}
	old_arm_q5=${8}
}

# Lunch File: Robot Initial Positions
up_old_third_task_launch_var () {
	# Initial Launch Values
	old_third_task_q1=${1}
	old_third_task_q2=${2}
	old_third_task_q3=${3}
	old_third_task_q4=${4}
	old_third_task_q5=${5}
}

# Set new folder name to write files in
up_folder_name_str () {
	if [ ${2} == "initialize" ]; then	
		echo "-> initializing folder..."
		str_f_old="\"folder_name_\"         default=\"\""
		str_f_new="\"folder_name_\"         default=\""${3}"/"${4}"\""
		sed -i -e "s/$str_f_old/$(echo $str_f_new | sed -e 's/\\/\\\\/g' -e 's/\//\\\//g' -e 's/&/\\\&/g')/g" ${1}
	elif [ ${2} == "reset" ]; then
		echo "-> reset folder..."
		str_f_old="\"folder_name_\"         default=\""${3}"/"${4}"\""
		str_f_new="\"folder_name_\"         default=\"\""
		sed -i -e "s/$(echo $str_f_old | sed -e 's/\\/\\\\/g' -e 's/\//\\\//g' -e 's/&/\\\&/g')/$str_f_new/g" ${1}
	elif [ ${2} == "set" ]; then
		str_f_old="\"folder_name_\"         default=\""${3}"/"${4}"\""
		let Cnew=${4}+1
		str_f_new="\"folder_name_\"         default=\""${3}"/"${Cnew}"\""
		sed -i -e "s/$(echo $str_f_old | sed -e 's/\\/\\\\/g' -e 's/\//\\\//g' -e 's/&/\\\&/g')/$(echo $str_f_new | sed -e 's/\\/\\\\/g' -e 's/\//\\\//g' -e 's/&/\\\&/g')/g" ${1}
	fi
}

# Set new Robot initial pose in launch file
up_launch_robot_ini_pose () {
	if [ ${2} == "reset" ]; then
		str_quad_x_new="\"quad_x_ini_\"         default=\"0.0\"" 
	 	str_quad_y_new="\"quad_y_ini_\"         default=\"0.0\"" 
		str_quad_z_new="\"quad_z_ini_\"         default=\"0.0\"" 
		str_arm_q1_new="\"arm_q1_ini_\"         default=\"0.0\"" 
		str_arm_q2_new="\"arm_q2_ini_\"         default=\"0.0\"" 
		str_arm_q3_new="\"arm_q3_ini_\"         default=\"0.0\"" 
		str_arm_q4_new="\"arm_q4_ini_\"         default=\"0.0\"" 
		str_arm_q5_new="\"arm_q5_ini_\"         default=\"0.0\"" 
	elif [ ${2} == "set" ]; then
		#statements
 		str_quad_x_new="\"quad_x_ini_\"         default=\""${3}"\"" 
 		str_quad_y_new="\"quad_y_ini_\"         default=\""${4}"\"" 
		str_quad_z_new="\"quad_z_ini_\"         default=\""${5}"\"" 
		str_arm_q1_new="\"arm_q1_ini_\"         default=\""${6}"\"" 
		str_arm_q2_new="\"arm_q2_ini_\"         default=\""${7}"\"" 
		str_arm_q3_new="\"arm_q3_ini_\"         default=\""${8}"\"" 
		str_arm_q4_new="\"arm_q4_ini_\"         default=\""${9}"\"" 
		str_arm_q5_new="\"arm_q5_ini_\"         default=\""${10}"\"" 
	fi

	str_quad_x_old="\"quad_x_ini_\"         default=\""$old_quad_x"\""  
	str_quad_y_old="\"quad_y_ini_\"         default=\""$old_quad_y"\""  
	str_quad_z_old="\"quad_z_ini_\"         default=\""$old_quad_z"\""  
	str_arm_q1_old="\"arm_q1_ini_\"         default=\""$old_arm_q1"\""  
	str_arm_q2_old="\"arm_q2_ini_\"         default=\""$old_arm_q2"\""  
	str_arm_q3_old="\"arm_q3_ini_\"         default=\""$old_arm_q3"\""  
	str_arm_q4_old="\"arm_q4_ini_\"         default=\""$old_arm_q4"\""  
	str_arm_q5_old="\"arm_q5_ini_\"         default=\""$old_arm_q5"\""  

	sed -i -e "s/$str_quad_x_old/$str_quad_x_new/g" ${1}
	sed -i -e "s/$str_quad_y_old/$str_quad_y_new/g" ${1}
	sed -i -e "s/$str_quad_z_old/$str_quad_z_new/g" ${1}
	sed -i -e "s/$str_arm_q1_old/$str_arm_q1_new/g" ${1}
	sed -i -e "s/$str_arm_q2_old/$str_arm_q2_new/g" ${1}
	sed -i -e "s/$str_arm_q3_old/$str_arm_q3_new/g" ${1}
	sed -i -e "s/$str_arm_q4_old/$str_arm_q4_new/g" ${1}
	sed -i -e "s/$str_arm_q5_old/$str_arm_q5_new/g" ${1}
}

# Set new Arm third task desired pose in launch file
up_launch_arm_third_task () {
	if [ ${2} == "reset" ]; then
		str_third_task_q1_new="\"third_task_q1_\"        default=\"0.0\"" 
	 	str_third_task_q2_new="\"third_task_q2_\"        default=\"0.2812\"" 
		str_third_task_q3_new="\"third_task_q3_\"        default=\"0.908\"" 
		str_third_task_q4_new="\"third_task_q4_\"        default=\"1.975\"" 
		str_third_task_q5_new="\"third_task_q5_\"        default=\"0.0\"" 
	elif [ ${2} == "set" ]; then
		#statements
		str_third_task_q1_new="\"third_task_q1_\"        default=\""${3}"\"" 
	 	str_third_task_q2_new="\"third_task_q2_\"        default=\""${4}"\"" 
		str_third_task_q3_new="\"third_task_q3_\"        default=\""${5}"\"" 
		str_third_task_q4_new="\"third_task_q4_\"        default=\""${6}"\"" 
		str_third_task_q5_new="\"third_task_q5_\"        default=\""${7}"\"" 
	fi

	str_third_task_q1_old="\"third_task_q1_\"        default=\""$old_third_task_q1"\""  
	str_third_task_q2_old="\"third_task_q2_\"        default=\""$old_third_task_q2"\""  
	str_third_task_q3_old="\"third_task_q3_\"        default=\""$old_third_task_q3"\""  
	str_third_task_q4_old="\"third_task_q4_\"        default=\""$old_third_task_q4"\""  
	str_third_task_q5_old="\"third_task_q5_\"        default=\""$old_third_task_q5"\""  

	sed -i -e "s/$str_third_task_q1_old/$str_third_task_q1_new/g" ${1}
	sed -i -e "s/$str_third_task_q2_old/$str_third_task_q2_new/g" ${1}
	sed -i -e "s/$str_third_task_q3_old/$str_third_task_q3_new/g" ${1}
	sed -i -e "s/$str_third_task_q4_old/$str_third_task_q4_new/g" ${1}
	sed -i -e "s/$str_third_task_q5_old/$str_third_task_q5_new/g" ${1}
}

check_experiment () {

	# Check if the files are saved
	file=${USER_HOME}"/"${subfoldername}"/"${COUNTER_third_task}"/q_values.txt"

	if [ ! -f "$file" ]
	then
    	echo "Something with software goes wrong. Aborting..."
    	# Kill Ros launched nodes
		ps -ef | grep "roslaunch" | awk '{print $2}' | xargs kill
	else
		# Let max 250 sec to the experiment execution 
		for ii in {1..250}
		do
			if pgrep roslaunch > /dev/null ; then
				if pgrep gazebo > /dev/null ; then
					echo "Running"
					sleep 1
				else
					echo "Gazebo fatal error"
					return
				fi
			else
				echo "Experiment Finished"
				return
			fi
		done

		echo "Experiment time out, aborting..."

		# Kill Ros launched nodes
		ps -ef | grep "roslaunch" | awk '{print $2}' | xargs kill
	fi
}

# Get User Home directory
USER_HOME=$(eval echo ~${SUDO_USER})
USER_HOME=${USER_HOME}
echo "User Home directory: "${USER_HOME}

# Main folder name
Rootpath="/Desktop/Results"

# Launch relative path
Lpath="../launch/kinton_arm_task_priority_ctrl_sim.launch"

# Poses file name
filename="exp_poses.txt"
filename_third_task="exp_third_task.txt"

# Open Roscore
roscore "on"

COUNTER=0

mainfolder=$Rootpath"/third_task"
mkdir ${USER_HOME}"/"${mainfolder}
foldername=${mainfolder}"/Ahierarchic"
mkdir ${USER_HOME}"/"${foldername}

#copy config file in folder to know the setting
cp ${filename} ${USER_HOME}"/"${mainfolder}"/."
cp ${filename_third_task} ${USER_HOME}"/"${mainfolder}"/."

up_old_launch_var 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0

while read quad_x quad_y quad_z arm_q1 arm_q2 arm_q3 arm_q4 arm_q5
do
    [ "$quad_x" == "Quad_x" ] && continue                       # Skip the Header
    echo "New Values: " $quad_x $quad_y $quad_z $arm_q1 $arm_q2 $arm_q3 $arm_q4 $arm_q5       # Print values read in variable

    let COUNTER=${COUNTER}+1

    subfoldername=${foldername}"/"${COUNTER}
    # create new folder
    mkdir ${USER_HOME}"/"${subfoldername}

    # Set launch file initial robot pose
    up_launch_robot_ini_pose $Lpath "set" $quad_x $quad_y $quad_z $arm_q1 $arm_q2 $arm_q3 $arm_q4 $arm_q5

    COUNTER_third_task=0

    up_old_third_task_launch_var 0.0 0.2812 0.908 1.975 0.0

	up_folder_name_str $Lpath "initialize" ${subfoldername} ${COUNTER_third_task}

	while read arm_q1_third_task arm_q2_third_task arm_q3_third_task arm_q4_third_task arm_q5_third_task
	do

    	[ "$arm_q1_third_task" == "Arm_q1" ] && continue                       # Skip the Header
    	echo "New Values: " $arm_q1_third_task $arm_q2_third_task $arm_q3_third_task $arm_q4_third_task $arm_q5_third_task       # Print values read in variable

	    up_folder_name_str $Lpath "set" ${subfoldername} ${COUNTER_third_task}

	    let COUNTER_third_task=${COUNTER_third_task}+1

    	# create new folder
    	mkdir ${USER_HOME}"/"${subfoldername}"/"${COUNTER_third_task}

		up_launch_arm_third_task $Lpath "set" $arm_q1_third_task $arm_q2_third_task $arm_q3_third_task $arm_q4_third_task $arm_q5_third_task

		# Launch ROS nodes
		x-terminal-emulator -e "roslaunch kinton_arm_task_priority_control kinton_arm_task_priority_ctrl_sim_sh.launch" &
	
		# Let 30 sec. to start the nodes and writes the files	
		sleep 30
			
		check_experiment
			
		up_old_third_task_launch_var $arm_q1_third_task $arm_q2_third_task $arm_q3_third_task $arm_q4_third_task $arm_q5_third_task

	done < $filename_third_task

	# reset folder name
	up_folder_name_str $Lpath "reset" ${subfoldername} ${COUNTER_third_task}

	# Reset launch arm third task pose
	up_launch_arm_third_task $Lpath "reset"

	# Update the last changed vars in launch file
	up_old_launch_var $quad_x $quad_y $quad_z $arm_q1 $arm_q2 $arm_q3 $arm_q4 $arm_q5
done < $filename

# reset folder name
up_folder_name_str $Lpath "reset" ${foldername} ${COUNTER}

# Reset launch robot initial pose
up_launch_robot_ini_pose $Lpath "reset"

roscore "off"