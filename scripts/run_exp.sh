#!/bin/bash

# Open or close roscore
roscore () {
	if [ ${1} == "on" ]; then
		# Open Roscore
		x-terminal-emulator -e "roscore" &
		# Wait time
		sleep 5
	elif [ ${1} == "off" ]; then
		# kill roscore
		ps -ef | grep "roscore" | awk '{print $2}' | xargs kill
	fi
}

# Enable or Disable secondary task in Launch file
up_enable_sec_task () {
	if [ ${2} == "true" ]; then
		str_activate_old="\"enable_sec_task_\"       default=\"false\""  
		str_activate_new="\"enable_sec_task_\"       default=\"${2}\"" 
		sed -i -e "s/$str_activate_old/$str_activate_new/g" ${1}
	else
		str_activate_old="\"enable_sec_task_\"       default=\"true\""  
		str_activate_new="\"enable_sec_task_\"       default=\"${2}\"" 
		sed -i -e "s/$str_activate_old/$str_activate_new/g" ${1}
	fi
}

# Lunch File: Robot Initial Positions
up_old_launch_var () {
	# Initial Launch Values
	old_quad_x=${1}
	old_quad_y=${2}
	old_quad_z=${3}
	old_arm_q1=${4}
	old_arm_q2=${5}
	old_arm_q3=${6}
	old_arm_q4=${7}
	old_arm_q5=${8}
}

# Set new folder name to write files in
up_folder_name_str () {
	if [ ${2} == "initialize" ]; then	
		echo "-> initializing folder..."
		str_f_old="\"folder_name_\"         default=\"\""
		str_f_new="\"folder_name_\"         default=\""${3}"/"${4}"\""
		sed -i -e "s/$str_f_old/$(echo $str_f_new | sed -e 's/\\/\\\\/g' -e 's/\//\\\//g' -e 's/&/\\\&/g')/g" ${1}

	elif [ ${2} == "reset" ]; then
		echo "-> reset folder..."
		str_f_old="\"folder_name_\"         default=\""${3}"/"${4}"\""
		str_f_new="\"folder_name_\"         default=\"\""
		sed -i -e "s/$(echo $str_f_old | sed -e 's/\\/\\\\/g' -e 's/\//\\\//g' -e 's/&/\\\&/g')/$str_f_new/g" ${1}
	elif [ ${2} == "set" ]; then
		str_f_old="\"folder_name_\"         default=\""${3}"/"${4}"\""
		let COUNTER=COUNTER+1
		str_f_new="\"folder_name_\"         default=\""${3}"/"${COUNTER}"\""
		sed -i -e "s/$(echo $str_f_old | sed -e 's/\\/\\\\/g' -e 's/\//\\\//g' -e 's/&/\\\&/g')/$(echo $str_f_new | sed -e 's/\\/\\\\/g' -e 's/\//\\\//g' -e 's/&/\\\&/g')/g" ${1}
	fi
}

# Set new Robot initial pose in launch file
up_launch_robot_ini_pose () {
	if [ ${2} == "reset" ]; then
		str_quad_x_new="\"quad_x_ini_\"         default=\"0.0\"" 
	 	str_quad_y_new="\"quad_y_ini_\"         default=\"0.0\"" 
		str_quad_z_new="\"quad_z_ini_\"         default=\"0.0\"" 
		str_arm_q1_new="\"arm_q1_ini_\"         default=\"0.0\"" 
		str_arm_q2_new="\"arm_q2_ini_\"         default=\"0.0\"" 
		str_arm_q3_new="\"arm_q3_ini_\"         default=\"0.0\"" 
		str_arm_q4_new="\"arm_q4_ini_\"         default=\"0.0\"" 
		str_arm_q5_new="\"arm_q5_ini_\"         default=\"0.0\"" 
	elif [ ${2} == "set" ]; then
		#statements
 		str_quad_x_new="\"quad_x_ini_\"         default=\""${3}"\"" 
 		str_quad_y_new="\"quad_y_ini_\"         default=\""${4}"\"" 
		str_quad_z_new="\"quad_z_ini_\"         default=\""${5}"\"" 
		str_arm_q1_new="\"arm_q1_ini_\"         default=\""${6}"\"" 
		str_arm_q2_new="\"arm_q2_ini_\"         default=\""${7}"\"" 
		str_arm_q3_new="\"arm_q3_ini_\"         default=\""${8}"\"" 
		str_arm_q4_new="\"arm_q4_ini_\"         default=\""${9}"\"" 
		str_arm_q5_new="\"arm_q5_ini_\"         default=\""${10}"\"" 
	fi

	str_quad_x_old="\"quad_x_ini_\"         default=\""$old_quad_x"\""  
	str_quad_y_old="\"quad_y_ini_\"         default=\""$old_quad_y"\""  
	str_quad_z_old="\"quad_z_ini_\"         default=\""$old_quad_z"\""  
	str_arm_q1_old="\"arm_q1_ini_\"         default=\""$old_arm_q1"\""  
	str_arm_q2_old="\"arm_q2_ini_\"         default=\""$old_arm_q2"\""  
	str_arm_q3_old="\"arm_q3_ini_\"         default=\""$old_arm_q3"\""  
	str_arm_q4_old="\"arm_q4_ini_\"         default=\""$old_arm_q4"\""  
	str_arm_q5_old="\"arm_q5_ini_\"         default=\""$old_arm_q5"\""  

	sed -i -e "s/$str_quad_x_old/$str_quad_x_new/g" ${1}
	sed -i -e "s/$str_quad_y_old/$str_quad_y_new/g" ${1}
	sed -i -e "s/$str_quad_z_old/$str_quad_z_new/g" ${1}
	sed -i -e "s/$str_arm_q1_old/$str_arm_q1_new/g" ${1}
	sed -i -e "s/$str_arm_q2_old/$str_arm_q2_new/g" ${1}
	sed -i -e "s/$str_arm_q3_old/$str_arm_q3_new/g" ${1}
	sed -i -e "s/$str_arm_q4_old/$str_arm_q4_new/g" ${1}
	sed -i -e "s/$str_arm_q5_old/$str_arm_q5_new/g" ${1}
}

check_experiment () {
	# Let max 250 sec to the experiment execution 
	for ii in {1..250}
	do
		if pgrep roslaunch > /dev/null ; then
			if pgrep gazebo > /dev/null ; then
				echo "Running"
				sleep 1
			else
				echo "Gazebo fatal error"
				return
			fi
		else
			echo "Experiment Finished"
			return
		fi
	done

	echo "Experiment time out, aborting..."

	# Kill Ros launched nodes
	ps -ef | grep "roslaunch" | awk '{print $2}' | xargs kill
}

# Get User Home directory
USER_HOME=$(eval echo ~${SUDO_USER})
USER_HOME=${USER_HOME}
echo "User Home directory: "${USER_HOME}

# Main folder name
Rootpath="/Desktop/Results"

# Launch relative path
Lpath="../launch/kinton_arm_task_priority_ctrl_sim.launch"

# Poses file name
filename="exp_poses.txt"

# Open Roscore
roscore "on"

# for ii in 1 2
# do

	COUNTER=0

	# if [ "$ii" -eq 1 ]; then

	 	# echo "Non TPC" 
	 	# mainfolder=${Rootpath}"/nontpc"
 	 # 	mkdir ${USER_HOME}"/"${mainfolder}
	 	# foldername=${mainfolder}
	 	
	 	# up_folder_name_str $Lpath "initialize" ${foldername} ${COUNTER}

	# elif [ "$ii" -eq 2 ]; then
	 	echo "TPC Hierarchic"	
	 	mainfolder=$Rootpath"/tpc"
 	 	mkdir ${USER_HOME}"/"${mainfolder}
	 	foldername=${mainfolder}"/nonhierarchic"

	 	mkdir ${USER_HOME}"/"${foldername}
		
		up_folder_name_str $Lpath "initialize" ${foldername} ${COUNTER}

 		up_enable_sec_task $Lpath "true"

	# elif [ "$ii" -eq 3 ]; then
	# 	echo "TPC non Hierarchic"
	# 	mainfolder=$Rootpath"/tpc"
	#  	# mkdir ${USER_HOME}"/"${mainfolder} # It has been created in step ii=2
	#  	foldername=${mainfolder}"/nonhierarchic"
 # 	 	mkdir ${USER_HOME}"/"${foldername}
	 	
	#  	up_folder_name_str $Lpath "initialize" ${foldername} ${COUNTER}
	# fi

	up_old_launch_var 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0

	while read quad_x quad_y quad_z arm_q1 arm_q2 arm_q3 arm_q4 arm_q5
	do

	    [ "$quad_x" == "Quad_x" ] && continue                       # Skip the Header
	    echo "New Values: " $quad_x $quad_y $quad_z $arm_q1 $arm_q2 $arm_q3 $arm_q4 $arm_q5       # Print values read in variable

	    up_folder_name_str $Lpath "set" ${foldername} ${COUNTER}

	    # create new folder
	    mkdir ${USER_HOME}"/"${foldername}"/"${COUNTER}

 	    # Set launch file initial robot pose
	    up_launch_robot_ini_pose $Lpath "set" $quad_x $quad_y $quad_z $arm_q1 $arm_q2 $arm_q3 $arm_q4 $arm_q5

	    # Check if not ended the list of poses
		if [ "$quad_z" != "$ini_quad_z" ]; then

# 	  		# Launch ROS nodes
			x-terminal-emulator -e "roslaunch kinton_arm_task_priority_control kinton_arm_task_priority_ctrl_sim.launch" &
	
			# Let 20 sec. to start the nodes	
			sleep 20
			
			check_experiment
			
		fi


		# Update the last changed vars in launch file
		up_old_launch_var $quad_x $quad_y $quad_z $arm_q1 $arm_q2 $arm_q3 $arm_q4 $arm_q5

	done < $filename

	# reset folder name
 	up_folder_name_str $Lpath "reset" ${foldername} ${COUNTER}

# 	# Reset launch robot initial pose
	up_launch_robot_ini_pose $Lpath "reset"

# done

up_enable_sec_task $Lpath "false"

roscore "off"

