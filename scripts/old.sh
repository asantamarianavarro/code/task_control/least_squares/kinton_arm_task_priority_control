#!/bin/bash

x-terminal-emulator -e "roscore" &

# Wait time
sleep 10


filename="exp_poses.txt"

ini_quad_x="0.0"
ini_quad_y="0.0"
ini_quad_z="0.0"
ini_arm_q1="0.0"
ini_arm_q2="0.0"
ini_arm_q3="0.0"
ini_arm_q4="0.0"
ini_arm_q5="0.0"


for ii in 1 2
do

	if [ "$ii" -lt 2 ]; then

	 	echo "Non Hierarchic" 

	else
	 	echo "Hierarchic"	

		mkdir /home/asantamaria/Desktop/Results/nonhierarchic
		mv /home/asantamaria/Desktop/Results/* /home/asantamaria/Desktop/Results/nonhierarchic/.

		str_activate_old="\"enable_sec_task_\"       default=\"false\""  
		str_activate_new="\"enable_sec_task_\"       default=\"true\"" 
		sed -i -e "s/$str_activate_old/$str_activate_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
	fi

	# Initial Launch Values
	old_quad_x=$ini_quad_x
	old_quad_y=$ini_quad_y
	old_quad_z=$ini_quad_z
	old_arm_q1=$ini_arm_q1
	old_arm_q2=$ini_arm_q2
	old_arm_q3=$ini_arm_q3
	old_arm_q4=$ini_arm_q4
	old_arm_q5=$ini_arm_q5

	COUNTER=0

	while read quad_x quad_y quad_z arm_q1 arm_q2 arm_q3 arm_q4 arm_q5
	do

	        [ "$quad_x" == "Quad_x" ] && continue                       # Skip the Header
	        echo "New Values: " $quad_x $quad_y $quad_z $arm_q1 $arm_q2 $arm_q3 $arm_q4 $arm_q5       # Print values read in variable

	        new_quad_x=$quad_x
			new_quad_y=$quad_y
			new_quad_z=$quad_z
			new_arm_q1=$arm_q1
			new_arm_q2=$arm_q2
			new_arm_q3=$arm_q3
			new_arm_q4=$arm_q4
			new_arm_q5=$arm_q5

			str_folder_old="\"folder_name_\"         default=\""$COUNTER"\""
			let COUNTER=COUNTER+1
			str_folder_new="\"folder_name_\"         default=\""$COUNTER"\""

			str_quad_x_old="\"quad_x_ini_\"         default=\""$old_quad_x"\""  
			str_quad_x_new="\"quad_x_ini_\"         default=\""$new_quad_x"\"" 
			str_quad_y_old="\"quad_y_ini_\"         default=\""$old_quad_y"\""  
			str_quad_y_new="\"quad_y_ini_\"         default=\""$new_quad_y"\"" 
			str_quad_z_old="\"quad_z_ini_\"         default=\""$old_quad_z"\""  
			str_quad_z_new="\"quad_z_ini_\"         default=\""$new_quad_z"\"" 
			str_arm_q1_old="\"arm_q1_ini_\"         default=\""$old_arm_q1"\""  
			str_arm_q1_new="\"arm_q1_ini_\"         default=\""$new_arm_q1"\"" 
			str_arm_q2_old="\"arm_q2_ini_\"         default=\""$old_arm_q2"\""  
			str_arm_q2_new="\"arm_q2_ini_\"         default=\""$new_arm_q2"\"" 
			str_arm_q3_old="\"arm_q3_ini_\"         default=\""$old_arm_q3"\""  
			str_arm_q3_new="\"arm_q3_ini_\"         default=\""$new_arm_q3"\"" 
			str_arm_q4_old="\"arm_q4_ini_\"         default=\""$old_arm_q4"\""  
			str_arm_q4_new="\"arm_q4_ini_\"         default=\""$new_arm_q4"\"" 
			str_arm_q5_old="\"arm_q5_ini_\"         default=\""$old_arm_q5"\""  
			str_arm_q5_new="\"arm_q5_ini_\"         default=\""$new_arm_q5"\"" 

			# Open and modify launch file
			sed -i -e "s/$str_folder_old/$str_folder_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
			sed -i -e "s/$str_quad_x_old/$str_quad_x_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
			sed -i -e "s/$str_quad_y_old/$str_quad_y_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
			sed -i -e "s/$str_quad_z_old/$str_quad_z_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
			sed -i -e "s/$str_arm_q1_old/$str_arm_q1_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
			sed -i -e "s/$str_arm_q2_old/$str_arm_q2_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
			sed -i -e "s/$str_arm_q3_old/$str_arm_q3_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
			sed -i -e "s/$str_arm_q4_old/$str_arm_q4_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
			sed -i -e "s/$str_arm_q5_old/$str_arm_q5_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch

			if [ "$quad_z" != "$ini_quad_z" ] 
			then

				# Launch ROS nodes
				x-terminal-emulator -e "roslaunch kinton_arm_task_priority_control kinton_arm_task_priority_ctrl_sim.launch" &
		
				sleep 20
				

				for ii in {1..50}
				do
					if pgrep roslaunch > /dev/null 
					then
	    				echo "Running"
	    				sleep 5
					else
	    				echo "Stopped"
	    				continue
					fi
				done

				# Wait time
				# sleep 200
				
				# Kill Ros launched nodes
				# ps -ef | grep "roslaunch" | awk '{print $2}' | xargs kill

			fi


			old_quad_x=$new_quad_x
			old_quad_y=$new_quad_y
			old_quad_z=$new_quad_z
			old_arm_q1=$new_arm_q1
			old_arm_q2=$new_arm_q2
			old_arm_q3=$new_arm_q3
			old_arm_q4=$new_arm_q4
			old_arm_q5=$new_arm_q5

			

	done < $filename

	# reset folder name
	str_folder_old="\"folder_name_\"         default=\""$COUNTER"\""
	str_folder_new="\"folder_name_\"         default=\"0\""

	# Open and modify launch file
	str_quad_x_old="\"quad_x_ini_\"         default=\""$old_quad_x"\""  
	str_quad_y_old="\"quad_y_ini_\"         default=\""$old_quad_y"\""  
	str_quad_z_old="\"quad_z_ini_\"         default=\""$old_quad_z"\""  
	str_arm_q1_old="\"arm_q1_ini_\"         default=\""$old_arm_q1"\""  
	str_arm_q2_old="\"arm_q2_ini_\"         default=\""$old_arm_q2"\""  
	str_arm_q3_old="\"arm_q3_ini_\"         default=\""$old_arm_q3"\""  
	str_arm_q4_old="\"arm_q4_ini_\"         default=\""$old_arm_q4"\""  
	str_arm_q5_old="\"arm_q5_ini_\"         default=\""$old_arm_q5"\""  

	str_quad_x_new="\"quad_x_ini_\"         default=\""$ini_quad_x"\"" 
	str_quad_y_new="\"quad_y_ini_\"         default=\""$ini_quad_y"\"" 
	str_quad_z_new="\"quad_z_ini_\"         default=\""$ini_quad_z"\"" 
	str_arm_q1_new="\"arm_q1_ini_\"         default=\""$ini_arm_q1"\"" 
	str_arm_q2_new="\"arm_q2_ini_\"         default=\""$ini_arm_q2"\"" 
	str_arm_q3_new="\"arm_q3_ini_\"         default=\""$ini_arm_q3"\"" 
	str_arm_q4_new="\"arm_q4_ini_\"         default=\""$ini_arm_q4"\"" 
	str_arm_q5_new="\"arm_q5_ini_\"         default=\""$ini_arm_q5"\"" 

	# Open and modify launch file
	sed -i -e "s/$str_folder_old/$str_folder_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
	sed -i -e "s/$str_folder_old/$str_folder_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
	sed -i -e "s/$str_quad_x_old/$str_quad_x_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
	sed -i -e "s/$str_quad_y_old/$str_quad_y_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
	sed -i -e "s/$str_quad_z_old/$str_quad_z_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
	sed -i -e "s/$str_arm_q1_old/$str_arm_q1_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
	sed -i -e "s/$str_arm_q2_old/$str_arm_q2_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
	sed -i -e "s/$str_arm_q3_old/$str_arm_q3_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
	sed -i -e "s/$str_arm_q4_old/$str_arm_q4_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch
	sed -i -e "s/$str_arm_q5_old/$str_arm_q5_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch


done

str_activate_old="\"enable_sec_task_\"       default=\"true\""  
str_activate_new="\"enable_sec_task_\"       default=\"false\"" 
sed -i -e "s/$str_activate_old/$str_activate_new/g" ../launch/kinton_arm_task_priority_ctrl_sim.launch

ps -ef | grep "roscore" | awk '{print $2}' | xargs kill