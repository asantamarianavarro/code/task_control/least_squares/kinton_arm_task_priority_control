cmake_minimum_required(VERSION 2.8.3)
project(kinton_arm_task_priority_control)

## Find catkin macros and libraries
find_package(catkin REQUIRED)
# ******************************************************************** 
#                 Add catkin additional components here
# ******************************************************************** 
find_package(catkin REQUIRED COMPONENTS roscpp tf iri_base_algorithm std_msgs sensor_msgs kinton_msgs actionlib trajectory_msgs visualization_msgs kdl_parser eigen_conversions ar_tools_msgs geometry_msgs)
# ******************************************************************** 
#           		  System DEPENDENCIES
# ******************************************************************** 
find_package(iriutils REQUIRED)
find_package(uam_task_ctrl REQUIRED)
find_package(Eigen3 REQUIRED)

SET(CMAKE_BUILD_TYPE release)

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)

# ******************************************************************** 
#           Add system and labrobotica dependencies here
# ******************************************************************** 
# find_package(<dependency> REQUIRED)

# ******************************************************************** 
#           Add topic, service and action definition here
# ******************************************************************** 
## Generate messages in the 'msg' folder
# add_message_files(
#   FILES
#   Message1.msg
#   Message2.msg
# )

## Generate services in the 'srv' folder
# add_service_files(
#   FILES
#   Service1.srv
#   Service2.srv
# )

## Generate actions in the 'action' folder
# add_action_files(
#   FILES
#   Action1.action
#   Action2.action
# )

## Generate added messages and services with any dependencies listed here
# generate_messages(
#   DEPENDENCIES
#   std_msgs  # Or other packages containing msgs
# )

# ******************************************************************** 
#                 Add the dynamic reconfigure file 
# ******************************************************************** 
generate_dynamic_reconfigure_options(cfg/KintonArmTaskPriorityControl.cfg)

# ******************************************************************** 
#                 Add run time dependencies here
# ********************************************************************
catkin_package(
#  INCLUDE_DIRS 
#  LIBRARIES 
# ******************************************************************** 
#            Add ROS and IRI ROS run time dependencies
# ******************************************************************** 
  CATKIN_DEPENDS roscpp tf iri_base_algorithm std_msgs sensor_msgs kinton_msgs actionlib trajectory_msgs visualization_msgs kdl_parser eigen_conversions ar_tools_msgs geometry_msgs
# ******************************************************************** 
#      Add system and labrobotica run time dependencies here
# ******************************************************************** 
  DEPENDS iriutils uam_task_ctrl Eigen
)

###########
## Build ##
###########

# ******************************************************************** 
#                   Add the include directories 
# ******************************************************************** 
include_directories(include ${catkin_INCLUDE_DIRS} ${iriutils_INCLUDE_DIR} ${uam_task_ctrl_INCLUDE_DIR} ${EIGEN3_INCLUDE_DIR})

# include_directories(${<dependency>_INCLUDE_DIR})

## Declare a cpp library
# add_library(${PROJECT_NAME} <list of source files>)

## Declare a cpp executable
add_executable(${PROJECT_NAME} src/kinton_arm_task_priority_control_alg.cpp src/kinton_arm_task_priority_control_alg_node.cpp)

# ******************************************************************** 
#                   Add the libraries
# ******************************************************************** 

# WARNING: The libraries names matters. In some systems orocos-kdl library creates problems when using Eigen::JacobiSVD implementation
target_link_libraries(${PROJECT_NAME} ${uam_task_ctrl_LIBRARY} ${iriutils_LIBRARY} ${catkin_LIBRARIES})

# target_link_libraries(${PROJECT_NAME} ${<dependency>_LIBRARY})

# ******************************************************************** 
#               Add message headers dependencies 
# ******************************************************************** 
# add_dependencies(${PROJECT_NAME} <msg_package_name>_generate_messages_cpp)
add_dependencies(${PROJECT_NAME} std_msgs_generate_messages_cpp)
add_dependencies(${PROJECT_NAME} sensor_msgs_generate_messages_cpp)
add_dependencies(${PROJECT_NAME} kinton_msgs_generate_messages_cpp)
add_dependencies(${PROJECT_NAME} geometry_msgs_generate_messages_cpp)
add_dependencies(${PROJECT_NAME} trajectory_msgs_generate_messages_cpp)
add_dependencies(${PROJECT_NAME} visualization_msgs_generate_messages_cpp)
add_dependencies(${PROJECT_NAME} ar_tools_msgs_generate_messages_cpp)

# ******************************************************************** 
#               Add dynamic reconfigure dependencies 
# ******************************************************************** 
add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS})

